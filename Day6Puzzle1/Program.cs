﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6Puzzle1
{
	class SpaceObject {
		public string name;
		public string parentName;
		public SpaceObject parent = null;
		public int distanceFromSan = -1;

		public int GetOrbitCount()
		{
			SpaceObject parent = this.parent;
			int count = 0;
			while (parent != null)
			{
				++count;
				parent = parent.parent;
			}
			return count;
		}
	}

	class Program
	{
		static void Main(string[] args)
		{

			string[] lines = File.ReadAllLines("input.txt");

			SpaceObject com = new SpaceObject();

			SpaceObject[] orbits = new SpaceObject[lines.Length];
			Dictionary<string, SpaceObject> orbitsByName = new Dictionary<string, SpaceObject>();
			orbitsByName.Add("COM", com);
			for (int i = 0; i < orbits.Length; ++i)
			{
				string line = lines[i];
				string[] names = line.Split(')');
				orbits[i] = new SpaceObject() { name = names[1], parentName = names[0], parent = null };
				orbitsByName.Add(orbits[i].name, orbits[i]);
			}

			// now link all the parents
			for (int i = 0; i < orbits.Length; ++i)
			{
				SpaceObject orbit = orbits[i];
				SpaceObject parentOrbit = orbitsByName[orbit.parentName];
				orbit.parent = parentOrbit;
			}

			// finally, count the orbits for each orbit
			int sum = 0;
			for (int i = 0; i < orbits.Length; ++i)
			{
				sum += orbits[i].GetOrbitCount();
			}

			Console.WriteLine("Checksum: " + sum);

			// now get YOU and SAN
			SpaceObject you = orbitsByName["YOU"];
			SpaceObject san = orbitsByName["SAN"];

			// first, count the distance from SAN for each object
			int distanceFromSan = 0;
			san = san.parent;
			while (san != null)
			{
				san.distanceFromSan = distanceFromSan;
				++distanceFromSan;
				san = san.parent;
			}

			// now, walk from YOU backwards until we meet distanceFromSan
			int distanceFromYou = 0;
			you = you.parent;
			while (you.distanceFromSan == -1)
			{
				++distanceFromYou;
				you = you.parent;
			}

			int totalDistance = you.distanceFromSan + distanceFromYou;
			Console.WriteLine("Total distance: " + totalDistance);

			
			Console.ReadKey();

		}
	}
}
