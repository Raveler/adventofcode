﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day18Puzzle1
{
	class Step
	{
		public int distance;
		public string keysCollected;
		public int robot;
	}

	class Tile {
		public Point loc;
		public char code;
		public bool isEmpty;
		public bool isKey;
		public bool isDoor;
		public int distance = int.MaxValue;
		public int robot = -1;
		public List<char> keysCollected = new List<char>();
		public List<char> doorsPassed = new List<char>();

		public Dictionary<string, Dictionary<char, Step>> pathCache = new Dictionary<string, Dictionary<char, Step>>();

		public void SetWall()
		{
			isEmpty = false;
			code = '#';
		}

		public Step GetPathTo(string unlockedKeys, char targetKey)
		{
			// can't find it - see if we can find a subset and copy it!
			if (!pathCache.ContainsKey(unlockedKeys))
			{
				Dictionary<char, Step> newCache = null;
				foreach (string candidateSubset in pathCache.Keys)
				{
					if (IsSubset(candidateSubset, unlockedKeys))
					{
						if (newCache == null) newCache = new Dictionary<char, Step>();
						foreach (char newTargetKey in pathCache[candidateSubset].Keys) {
							newCache[newTargetKey] = pathCache[candidateSubset][newTargetKey];
						}
					}
				}
				if (newCache != null) pathCache[unlockedKeys] = newCache;

				if (!pathCache.ContainsKey(unlockedKeys)) return null;
			}
			if (!pathCache[unlockedKeys].ContainsKey(targetKey)) return null;
			return pathCache[unlockedKeys][targetKey];
		}

		public static bool IsSubset(string subset, string unlockedKeys)
		{
			for (int i = 0; i < subset.Length; ++i)
			{
				if (!unlockedKeys.Contains(subset[i])) return false;
			}
			return true;
		}

		public void AddPathTo(string unlockedKeys, char targetKey, Step step)
		{
			if (!pathCache.ContainsKey(unlockedKeys)) pathCache.Add(unlockedKeys, new Dictionary<char, Step>());
			pathCache[unlockedKeys][targetKey] = step;
		}
	}

	class Program
	{
		static Dictionary<Point, Tile> tiles = new Dictionary<Point, Tile>();

		static Dictionary<char, Tile> tilesPerKey = new Dictionary<char, Tile>();


		static void Main(string[] args)
		{
			var tiles = Program.tiles;
			string[] lines = File.ReadAllLines("input.txt");

			// read the map
			int height = lines.Length;
			int width = lines[0].Length;
			Point loc = new Point(-1, -1);
			int keyCount = 0;
			string allKeys = "";
			for (int y = height - 1; y >= 0; --y)
			{
				string line = lines[y];
				for (int x = 0; x < width; ++x)
				{
					Tile tile = new Tile() { loc = new Point(x, y), code = line[x] };
					tiles.Add(tile.loc, tile);
					if (tile.code == '@') loc = new Point(tile.loc.x, tile.loc.y);
					if (tile.code == '.' || tile.code == '@') tile.isEmpty = true;
					if (((int)'A') <= tile.code && tile.code <= ((int)'Z')) tile.isDoor = true;
					if (((int)'a') <= tile.code && tile.code <= ((int)'z')) tile.isKey = true;
					if (tile.isKey)
					{
						++keyCount;
						allKeys += tile.code;
						tilesPerKey[tile.code] = tile;
					}
				}
			}

			bool mustFix = true;

			Point[] robots;
			if (mustFix)
			{
				// change the vaults
				tiles[new Point(loc.x - 1, loc.y)].SetWall();
				tiles[new Point(loc.x + 1, loc.y)].SetWall();
				tiles[new Point(loc.x, loc.y - 1)].SetWall();
				tiles[new Point(loc.x, loc.y + 1)].SetWall();
				tiles[new Point(loc.x, loc.y)].SetWall();

				robots = new Point[] {
					new Point(loc.x - 1, loc.y - 1),
					new Point(loc.x + 1, loc.y - 1),
					new Point(loc.x - 1, loc.y + 1),
					new Point(loc.x + 1, loc.y + 1),
				};
				tiles[robots[0]].code = '@';
				tiles[robots[1]].code = '@';
				tiles[robots[2]].code = '@';
				tiles[robots[3]].code = '@';
			}
			else
			{
				robots = tiles.Values.Where(x => x.code == '@').Select(x => x.loc).ToArray();
			}
			RemoveWalls(tiles);

			for (int i = 0; i < robots.Length; ++i)
			{
				AssignRobots(i, robots[i]);
			}

			allKeys = String.Concat(allKeys.OrderBy(x => x));
			// count the paths from each key to all other keys
			/*for (int i = 0; i < allKeys.Length; ++i)
			{
				Console.WriteLine("Calculate " + i + "...");
				CalculatePathsTo(tilesPerKey[allKeys[i]].loc, allKeys);
			}

			for (int i = 0; i < robots.Length; ++i)
			{
				tiles[robots[i]].robot = i;
				CalculatePathsTo(robots[i], allKeys);
			}


			string json = Newtonsoft.Json.JsonConvert.SerializeObject(tiles.Values);
			File.WriteAllText("pathcache.txt", json);*/


			var cache = JsonConvert.DeserializeObject<Tile[]>(File.ReadAllText("pathcache.txt"));
			foreach (Tile tile in cache)
			{
				tiles[tile.loc].pathCache = tile.pathCache;
			}

			var tpk = tilesPerKey;

			// get the points of interest
			List<char> keys = new List<char>();
			Console.WriteLine("Try to collect " + keyCount + " keys...");
			int steps = ExploreMazeEfficiently(robots, "", allKeys, 0);

			var fCalls = Program.fCalls;
			Console.WriteLine("Done in " + steps + " steps");



			Console.WriteLine("DONE");
			Console.ReadKey();
		}

		private static void RemoveWalls(Dictionary<Point, Tile> tiles)
		{
			var keys = tiles.Keys.ToList();
			foreach (var key in keys)
			{
				if (tiles[key].code == '#') tiles.Remove(key);
			}
		}

		static int n = 0;

		static int CompareTile(Tile a, Tile b)
		{
			return a.distance - b.distance;
		}


		static Dictionary<string, int> fCalls = new Dictionary<string, int>();

		static int ExploreMazeEfficiently(Point[] robots, string keys, string allKeys, int depth, int bestSoFar = int.MaxValue)
		{
			if (depth > allKeys.Length) return int.MaxValue;
			if (depth < 8) Console.WriteLine(String.Concat(Enumerable.Repeat("  ", depth)) + depth + " Explore " + keys + " keys");

			string hash = robots[0].ToString() + " " + robots[1].ToString() + " " + robots[2].ToString() + " " + robots[3].ToString() + " " + keys;
			if (fCalls.ContainsKey(hash)) return fCalls[hash];

			if (depth == 2)
			{
				int z = 5;
			}

			// try each robot
			int bestDistance = int.MaxValue;
			for (int i = 0; i < robots.Length; ++i)
			{
				Point startLoc = robots[i];
				Tile start = tiles[startLoc];
				//Console.WriteLine("Try robot " + i + " at " + startLoc);

				// go over all other keys and see if we can reach them
				for (int targetKeyIndex = 0; targetKeyIndex < allKeys.Length; ++targetKeyIndex)
				{
					char targetKey = allKeys[targetKeyIndex];

					// already got the key
					if (keys.Contains(targetKey)) continue;

					// see if we can reach the target key from here with the keys we have
					Step step = start.GetPathTo(keys, targetKey);

					// we can't reach it!
					if (step == null) continue;
					//Console.WriteLine("Found step for robot " + step.robot + " to key " + step.keysCollected);
					if (keys.Length == 3)
					{
						int bla = 3;
					}

					int distanceToTargetKey = step.distance;
					int newDistance = distanceToTargetKey;
					//Console.WriteLine("Getting to " + targetKey + " is distance " + newDistance);
					if (newDistance > bestDistance) continue;
					if (newDistance > bestSoFar) continue;

					string newKeys = keys;
					foreach (char newKey in step.keysCollected)
					{
						if (!newKeys.Contains(newKey)) newKeys += newKey;
					}
					newKeys = new string(newKeys.OrderBy(c => c).ToArray());
					//Console.WriteLine("New keys: " + newKeys);
					if (newKeys.Length == allKeys.Length)
					{
						//Console.WriteLine("Collected ALL!");
						if (!fCalls.ContainsKey(hash)) fCalls[hash] = newDistance;
						//Console.WriteLine("FINALDistance for " + hash + " is " + newDistance);
						return newDistance;
					}

					Point[] newRobots = new Point[] {
						robots[0],
						robots[1],
						robots[2],
						robots[3],
					};
					newRobots[step.robot] = tilesPerKey[targetKey].loc;

					int totalDistance = newDistance + ExploreMazeEfficiently(newRobots, newKeys, allKeys, depth + 1, bestDistance);
					if (totalDistance < bestDistance)
					{
						bestDistance = totalDistance;
					}
				}
			}

			//Console.WriteLine("bestDistance for " + hash + " is " + bestDistance);
			if (!fCalls.ContainsKey(hash)) fCalls[hash] = bestDistance;
			return bestDistance;
		}

		static void AssignRobots(int robot, Point start)
		{
			List<Point> closed = new List<Point>();
			closed.Add(start);
			while (closed.Count > 0)
			{
				Point next = closed[0];
				closed.RemoveAt(0);
				tiles[next].robot = robot;

				foreach (Point neighbourLoc in next.GetNeighbours())
				{
					if (!tiles.ContainsKey(neighbourLoc)) continue;
					Tile neighbour = tiles[neighbourLoc];
					if (neighbour.robot != -1) continue;
					if (neighbour.code == '#') continue;
					closed.Add(neighbourLoc);
				}
			}
		}

		static void CalculatePathsTo(Point startLoc, string allKeys)
		{
			// reset all tiles
			foreach (Tile tile in tiles.Values)
			{
				tile.distance = int.MaxValue;
				tile.doorsPassed.Clear();
				tile.keysCollected.Clear();
			}

			// do a dijkstra to figure out the fastest way to all available points
			List<Tile> open = new List<Tile>(tiles.Values.Where(x => x.isEmpty || x.isKey || x.isDoor));
			Tile start = tiles[startLoc];
			tiles[startLoc].distance = 0;
			while (open.Count > 0)
			{
				open.Sort(CompareTile);
				Tile next = open[0];

				// couldn't reach this tile!
				if (next.distance == int.MaxValue) break;

				open.RemoveAt(0);
				//Console.WriteLine("Tile " + next.loc + " is at distance " + next.distance);

				if (next.isDoor)
				{
					next.doorsPassed.Add((char)((int)next.code + 32));
				}

				if (next.isKey && next != start)
				{
					//Console.WriteLine("We found the shortest path to key " + next.code);
					next.keysCollected.Add(next.code);
				}

				if (next.isKey && next != start)
				{
					List<char> keysCollected = new List<char>();
					keysCollected.AddRange(next.keysCollected);
					Step step = new Step()
					{
						distance = next.distance,
						keysCollected = String.Concat(keysCollected),
						robot = next.robot,
					};
					Console.WriteLine("From key " + start.code + " to " + next.code + " for robot " + next.robot + " it is distance " + next.distance + " and " + String.Concat(next.doorsPassed) + " doors passed, " + step.keysCollected + " keys collected");
					start.AddPathTo(String.Concat(next.doorsPassed), next.code, step);
				}

				// don't pass through a key
				foreach (Point neighbourLoc in next.loc.GetNeighbours())
				{
					if (!tiles.ContainsKey(neighbourLoc)) continue;
					Tile neighbour = tiles[neighbourLoc];
					if (neighbour.code == '#') continue;
					if (neighbour.distance != int.MaxValue) continue;
					int distance = next.distance + 1;
					if (distance < neighbour.distance) neighbour.distance = distance;
					neighbour.doorsPassed.Clear();
					neighbour.doorsPassed.AddRange(next.doorsPassed);
					neighbour.keysCollected.Clear();
					neighbour.keysCollected.AddRange(next.keysCollected);
					//if (next.robot != -1) neighbour.robot = next.robot;
					//Console.WriteLine("Tile " + neighbour.loc + " is now at distance " + distance);
				}
			}
		}

	}
}
