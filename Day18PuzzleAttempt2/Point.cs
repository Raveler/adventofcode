﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

struct Point : IEquatable<Point>
{
	public int x;
	public int y;

	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public bool Equals(Point other)
	{
		return x == other.x && y == other.y;
	}

	public override int GetHashCode()
	{
		return (x, y).GetHashCode();
	}

	public IEnumerable<Point> GetNeighbours()
	{
		yield return new Point(x - 1, y);
		yield return new Point(x + 1, y);
		yield return new Point(x, y - 1);
		yield return new Point(x, y + 1);
	}

	public override string ToString()
	{
		return "[" + x + "," + y + "]";
	}
}