﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day24Puzzle1
{
	class Tile {
		public bool bug;
		public bool newBug;
	}

	class Level {
		public Tile[,] tiles;
	}

	class Planet
	{
		public Dictionary<int, Level> levels = new Dictionary<int, Level>();
	}

	class Program
	{
		static Tile[,] tiles;

		static int width = 5;
		static int height = 5;


		static void Main(string[] args)
		{

			string[] lines = File.ReadAllLines("input.txt");
			tiles = new Tile[width, height];
			for (int y = 0; y < height; ++y)
			{
				for (int x = 0; x < width; ++x)
				{
					string line = lines[height - 1 - y];
					char c = line[x];

					Tile tile = new Tile();
					tile.bug = (c == '#');
					tiles[x, y] = tile;
				}
			}
			Print(tiles);

			HashSet<long> tileHashes = new HashSet<long>();
			bool done = false;
			long hash = 0;
			long nLoops = 0;
			while (!done)
			{
				Evolve();
				++nLoops;
				hash = GetBiodiversity(tiles);
				if (tileHashes.Contains(hash)) break;
				else tileHashes.Add(hash);
			}

			Print(tiles);

			Console.WriteLine("After " + nLoops + " minutes, we have a biodiversity loop with score " + hash);

			Console.ReadKey();
		}

		static void Evolve()
		{
			for (int y = 0; y < height; ++y)
			{
				for (int x = 0; x < width; ++x)
				{
					Tile tile = tiles[x, y];
					int nBugs = GetNeighbourCount(x, y);
					if (tile.bug)
					{
						tile.newBug = (nBugs == 1);
					}
					else
					{
						tile.newBug = (nBugs == 1 || nBugs == 2);
					}
					//Console.WriteLine("Tile " + x + "," + y + " has " + tile.bug + " going to " + tile.newBug + " because " + nBugs + " neighbours");
				}
			}
			for (int y = 0; y < height; ++y)
			{
				for (int x = 0; x < width; ++x)
				{
					tiles[x, y].bug = tiles[x, y].newBug;
				}
			}
		}

		static int GetNeighbourCount(int x, int y)
		{
			int nBugs = 0;
			int dx = 0;
			int dy = 1;
			for (int i = 0; i < 4; ++i)
			{
				int tmp = dx;
				dx = -dy;
				dy = tmp;
				if (x + dx < 0 || x + dx >= width) continue;
				if (y + dy < 0 || y + dy >= height) continue;
				if (tiles[x + dx, y + dy].bug) ++nBugs;
			}

			return nBugs;
		}

		static long GetBiodiversity(Tile[,] tiles)
		{
			long multiplier = 1;
			long rating = 0;
			for (int y = height - 1; y >= 0; --y)
			{
				for (int x = 0; x < width; ++x)
				{
					if (tiles[x, y].bug) rating += multiplier;
					multiplier = multiplier << 1;
				}
			}
			return rating;
		}

		static void Print(Tile[,] tiles)
		{
			for (int y = height - 1; y >= 0; --y)
			{
				for (int x = 0; x < width; ++x)
				{
					Console.Write(tiles[x, y].bug ? '#' : '.');
				}
				Console.WriteLine();
			}
			Console.WriteLine();
		}
	}
}
