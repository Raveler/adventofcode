﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day23Puzzle1
{
	class Program
	{
		static void Main(string[] args)
		{
			long[] program = IntcodeComputer.ParseProgram("input.txt");

			int nComputers = 50;
			IntcodeComputer[] computers = new IntcodeComputer[nComputers];
			for (int i = 0; i < nComputers; ++i)
			{
				IntcodeComputer computer = new IntcodeComputer(program, new long[0]);
				computers[i] = computer;
				var response = computer.RunUntilInputRequested();
				if (response == IntCodeResponseType.Done) throw new Exception("Shouldn't be done!");
				computer.AddInput(i);
			}

			// now we are ready!
			Queue<long>[] inputQueues = new Queue<long>[nComputers];
			for (int i = 0; i < nComputers; ++i) inputQueues[i] = new Queue<long>();
			bool done = false;
			bool totallyDone = false;
			List<long> outputs = new List<long>();
			long[] natPacket = new long[2];
			bool hasNatPacket = false;
			long prevNatPacketY = long.MinValue;
			while (!done && !totallyDone)
			{
				// run each computer until it requests input
				done = true;
				bool allIdle = true;
				for (int i = 0; i < nComputers; ++i)
				{
					IntcodeComputer computer = computers[i];
					//Console.WriteLine("Run computer " + i + "...");
					Queue<long> queue = inputQueues[i];

					// calculate the computer and queue packets
					var response = computer.CalculateProgram();
					while (response.type == IntCodeResponseType.Output)
					{
						outputs.Add(response.value);
						done = false;
						allIdle = false;
						response = computer.CalculateProgram();
					}

					// process the outputs - queue them properly
					for (int outputIndex = 0; outputIndex < outputs.Count; outputIndex += 3)
					{
						long receiverIndex = outputs[outputIndex];
						long x = outputs[outputIndex + 1];
						long y = outputs[outputIndex + 2];
						if (receiverIndex == 255)
						{
							//Console.WriteLine("First packet sent to " + receiverIndex + " has Y " + y);
							natPacket[0] = x;
							natPacket[1] = y;
							hasNatPacket = true;
							continue;
						}
						IntcodeComputer receiver = computers[receiverIndex];
						receiver.AddInput(x);
						receiver.AddInput(y);
						//Console.WriteLine("Send from " + i + " to " + receiverIndex + ": x=" + x + ", y=" + y);
					}
					outputs.Clear();

					// more input needed
					if (response.type == IntCodeResponseType.InputNeeded)
					{
						done = false;

						// dequeue
						if (queue.Count == 0)
						{
							//Console.WriteLine("Queue for " + i + " is empty, add -1...");
							computer.AddInput(-1);
						}
						else
						{
							allIdle = false;
							//Console.WriteLine("Add " + queue.Count + " messages to " + i + "...");
							while (queue.Count > 0)
							{
								long input = queue.Dequeue();
								computer.AddInput(input);
							}

						}
					}
				}

				if (allIdle && hasNatPacket)
				{
					IntcodeComputer receiver = computers[0];
					long x = natPacket[0];
					long y = natPacket[1];
					receiver.AddInput(x);
					receiver.AddInput(y);
					Console.WriteLine("Send NAT packet to 0: " + x + "," + y);
					if (y == prevNatPacketY)
					{
						Console.WriteLine("FIRST NAT PACKET SENT TWICE: Y=" + y);
						totallyDone = true;
						break;
					}
					prevNatPacketY = y;
					hasNatPacket = false;
				}
			}


			Console.WriteLine("DONE!");
			Console.ReadKey();

		}
	}
}
