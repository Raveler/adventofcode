﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7Puzzle2
{
	class Program
	{
		private static List<int[]> allPhaseSettings = new List<int[]>();

		static void Main(string[] args)
		{
			string text = File.ReadAllText("input.txt");
			int[] program = text.Split(',').Select(x => int.Parse(x)).ToArray();

			// generate all permutations
			int[] initialPhaseSettings = new int[] { 5, 6, 7, 8, 9 };
			HeapPermutation(initialPhaseSettings, initialPhaseSettings.Length, initialPhaseSettings.Length, AddPhaseSetting);

			// go over all permutations and collect the highest output
			int bestOutput = int.MinValue;
			int[] bestPhaseSettings;

			foreach (var phaseSettings in allPhaseSettings)
			{
				// create intcode computers for each amplifier
				IntcodeComputer[] amplifiers = new IntcodeComputer[5];
				for (int i = 0; i < amplifiers.Length; ++i)
				{
					amplifiers[i] = new IntcodeComputer(program, new int[] { phaseSettings[i] });
				}

				// now we keep running each program until it outputs something
				int currentAmplifier = 0;
				amplifiers[currentAmplifier].AddInput(0);
				int nextInput = amplifiers[currentAmplifier].CalculateProgram();
				while (nextInput != -1)
				{
					currentAmplifier = (currentAmplifier + 1) % amplifiers.Length;
					//Console.WriteLine("Next input for " + currentAmplifier + " is " + nextInput);
					amplifiers[currentAmplifier].AddInput(nextInput);
					nextInput = amplifiers[currentAmplifier].CalculateProgram();
				}

				// now finish all not-done amplifiers as well
				for (int i = 0; i < amplifiers.Length; ++i)
				{
					if (!amplifiers[i].IsDone()) amplifiers[i].CalculateProgram();
				}

				int output = amplifiers[amplifiers.Length - 1].GetLastOutput();

				if (output > bestOutput)
				{
					bestOutput = output;
					bestPhaseSettings = phaseSettings;
				}
			}

			Console.WriteLine("The best signal is " + bestOutput);
			Console.ReadKey();

		}

		private static void AddPhaseSetting(int[] phases)
		{
			allPhaseSettings.Add(phases.ToArray());
		}

		// Generate permutations using Heap Algorithm 
		static void HeapPermutation(int[] a, int size, int n, Action<int[]> action)
		{
			// if size becomes 1 then prints the obtained 
			// permutation 
			if (size == 1)
			{
				action(a);
				return;
			}

			for (int i = 0; i < size; i++)
			{
				HeapPermutation(a, size - 1, n, action);

				// if size is odd, swap first and last 
				// element 
				if (size % 2 == 1) Swap(a, 0, size-1);

				// If size is even, swap ith and last 
				// element 
				else Swap(a, i, size-1);
			}
		}

		static void Swap(int[] a, int i1, int i2) {
			int v = a[i1];
			a[i1] = a[i2];
			a[i2] = v;
		}
	}
}
