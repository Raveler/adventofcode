﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5Puzzle1
{
	class Program
	{
		static void Main(string[] args)
		{
			string text = File.ReadAllText("input.txt");
			int[] codes = text.Split(',').Select(x => int.Parse(x)).ToArray();

			IntcodeComputer comp = new IntcodeComputer(codes, new int[] { 5 });
			comp.CalculateProgram();

			Console.ReadKey();
		}
	}
}

