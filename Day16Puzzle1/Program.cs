﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day16Puzzle1
{
	class Program
	{
		static int[,] buffer;

		static void Main(string[] args)
		{
			string line = File.ReadAllText("input.txt");
			//line = "02935109699940807407585447034323";
			int[] rawInput = new int[line.Length];
			for (int i = 0; i < line.Length; ++i)
			{
				rawInput[i] = int.Parse("" + line[i]);
			}

			// extend
			int nRepeats = 10000;
			int[] input = new int[rawInput.Length * nRepeats];
			for (long i = 0; i < input.Length; ++i)
			{
				input[i] = rawInput[i % rawInput.Length];
			}
			buffer = new int[input.Length, 2];
			Console.WriteLine("Input replicated to length " + input.Length);

			int[] pattern = new int[] { 0, 1, 0, -1 };

			// parse the offset message
			long offset = long.Parse(line.Substring(0, 7));
			//offset = 0;
			Console.WriteLine("Offset is " + offset + " which is " + ((float)offset / (float)input.Length * 100) + "% on the way.");


			int nPhases = 100;
			//Print(input);
			for (int i = 0; i < nPhases; ++i)
			{
				Console.WriteLine("Phase " + i + "...");
				if (offset > input.Length / 2) input = FastFFT(input, pattern, offset);
				else input = FFT(input, pattern, offset);
				//Print(input);
			}

			Print(input, offset);

			Console.WriteLine("DONE");
			Console.ReadKey();
		}

		static int[] FastFFT(int[] input, int[] pattern, long offset)
		{
			Console.WriteLine("Fast FFT shortcut!");
			int[] output = new int[input.Length];
			int sum = 0;
			for (int outputIdx = input.Length - 1; outputIdx >= offset; --outputIdx)
			{
				int inputIdx = outputIdx;
				long inputOffset = inputIdx - outputIdx;
				long patternIdx = ((inputOffset / (outputIdx + 1)) + 1) % pattern.Length;

				sum += input[inputIdx] * pattern[patternIdx];

				output[outputIdx] = (int)Math.Abs(sum) % 10;
			}

			return output;
		}

		static int[] FFT(int[] input, int[] pattern, long offset)
		{
			int[] output = new int[input.Length];

			// initialize to zero
			for (int i = 0; i < output.Length; ++i)
			{
				output[i] = 0;
			}

			for (long outputIdx = offset; outputIdx < input.Length; ++outputIdx)
			{
				output[outputIdx] = 0;

				// set the input idx to the first non-zero in the row
				long inputIdx = outputIdx;
				/*int patternIdx = 1;
				int patternOffset = 0;*/
				while (inputIdx < input.Length)
				{
					long inputOffset = inputIdx - outputIdx;
					long patternIdx = ((inputOffset / (outputIdx + 1)) + 1) % pattern.Length;
					/*if (patternIdx % 2 == 0)
					{
						inputIdx += (outputIdx + 1);
						continue;
					}*/

					//output[outputIdx] = (output[outputIdx] + input[inputIdx] * pattern[patternIdx]) % 10;
					output[outputIdx] += input[inputIdx] * pattern[patternIdx];
					//Console.WriteLine("inputOffset for " + inputIdx + " is " + inputOffset + ", and patternIdx is " + patternIdx + " resulting in " + input[inputIdx] + " * " + pattern[patternIdx]);
					Console.Write(input[inputIdx] + "*" + pattern[patternIdx] + " + ");
					++inputIdx;

					/*++patternOffset;
					if (patternOffset >= outputIdx + 1)
					{
						patternIdx = (patternIdx + 1) % pattern.Length;
						patternOffset = 0;
						if (patternIdx % 2 == 0)
						{
							patternIdx = (patternIdx + 1) % pattern.Length;
							inputIdx += (outputIdx + 1);
						}
					}*/
				}

				output[outputIdx] = (int)Math.Abs(output[outputIdx]) % 10;
				Console.WriteLine();
			}

			return output;
		}

		static void Print(int[] input, long offset)
		{
			for (int i = 0; i < 8; ++i)
			{
				Console.Write(input[i+offset]);
			}
			Console.WriteLine();
		}
	}
}
