﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day4Puzzle1
{
	class Program
	{
		const int N = 6;

		static int[] start = new int[] { 1, 8, 3, 5, 6, 4 };
		static int[] stop = new int[] { 6, 5, 7, 4, 7, 4 };

		static void Main(string[] args)
		{

			// start looking!
			int[] digits = start.ToArray();
			int nValid = 0;
			while (!IsDone(digits))
			{
				if (IsValid(digits)) ++nValid;

				// increase
				Increase(digits);
			}

			Console.WriteLine("There are " + nValid + " codes!");
			Console.ReadKey();
		}

		static bool IsDone(int[] digits)
		{
			for (int i = 0; i < N; ++i)
			{
				if (digits[i] != stop[i]) return false;
			}
			return true;
		}

		static bool IsValid(int[] digits)
		{
			int prev = digits[0];
			int prevSame = 1;
			bool hasDouble = false;
			for (int i = 1; i < N; ++i)
			{
				if (digits[i] < prev) return false;
				if (digits[i] == prev)
				{
					++prevSame;
				}
				else
				{
					if (prevSame == 2) hasDouble = true;
					prevSame = 1;
				}
				prev = digits[i];
			}
			if (prevSame == 2) hasDouble = true;
			return hasDouble;
		}

		static void Increase(int[] digits)
		{
			int i = N - 1;
			digits[i]++;
			while (digits[i] > 9)
			{
				digits[i] = 0;
				digits[i - 1]++;
				--i;
			}
		}
	}
}
