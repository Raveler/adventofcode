﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day22Puzzle1
{
	public enum Technique
	{
		DealIntoNewStack,
		Cut,
		DealWithIncrement
	}

	public class Action {
		public Technique technique;
		public int value;
		public long offset;
		public int multiplier;
	}

	public class Loop {
		public long start;
		public long length;
	}

	class Program
	{
		static void Main(string[] args)
		{
			string fileName = "input.txt";
			long nCards = 119315717514047;
			long cardToFollow = 2020;
			long nCycles = 101741582076661;

			/*string fileName = "easy-input.txt";
			long nCards = 10;
			long cardToFollow = 1;
			long nCycles = 5007;*/

			string[] lines = File.ReadAllLines(fileName);
			List<Action> actions = new List<Action>();
			Regex regex = new Regex("[0-9-]+");
			int multiplier = 1;
			long offset = 0;
			for (int i = 0; i < lines.Length; ++i)
			{
				string line = lines[i];

				Technique technique = Technique.DealIntoNewStack;
				if (line.Contains("cut"))
				{
					technique = Technique.Cut;
				}
				else if (line.Contains("increment"))
				{
					technique = Technique.DealWithIncrement;
				}
				else
				{
					technique = Technique.DealIntoNewStack;
				}

				int value = -1;
				if (technique != Technique.DealIntoNewStack)
				{
					value = int.Parse(regex.Match(line).Value);
				}

				/*if (technique == Technique.DealIntoNewStack)
				{
					multiplier = -multiplier;
					continue;
				}*/
				/*if (technique == Technique.Cut)
				{
					offset += value;
					continue;
				}*/


				Action action = new Action()
				{
					technique = technique,
					value = value,
					offset = offset,
					multiplier = multiplier,
				};
				actions.Add(action);
			}

			if (multiplier == -1) cardToFollow = nCards - 1 - cardToFollow;

			long originalCard = cardToFollow;

			// now invert the actions
			List<Action> invertedActions = new List<Action>();
			for (int i = actions.Count - 1; i >= 0; --i)
			{
				Action action = actions[i];
				Action newAction = new Action()
				{
					technique = action.technique,
					value = -action.value,
					offset = action.offset,
					multiplier = action.multiplier,
				};
				invertedActions.Add(newAction);
			}
			List<Action> originalActions = actions;
			actions = invertedActions;

			//long finalValue = PerformShuffle(actions, nCards, cardToFollow, nCycles);


			// do the actions
			/*long[] finalCards = new long[nCards];
			long[] finalInvertedCards = new long[nCards];
			for (int i = 0; i < nCards; ++i)
			{
				long finalPos = PerformShuffle(originalActions, nCards, i, nCycles);
				long finalValue = PerformShuffle(invertedActions, nCards, i, nCycles);
				finalCards[finalPos] = i;
				finalInvertedCards[i] = finalValue;
			}
			for (int i = 0; i < nCards; ++i)
			{
				Console.Write(finalCards[i] + " ");
			}
			Console.WriteLine();
			for (int i = 0; i < nCards; ++i)
			{
				Console.Write(finalInvertedCards[i] + " ");
			}
			Console.WriteLine();*/

			var loop = FindLoopLength(invertedActions, nCards, cardToFollow, nCycles);

			// first, determine the final value after the total number of loops
			/*long finalValueAtPosition = PerformShuffle(invertedActions, nCards, cardToFollow, nCycles);
			Console.WriteLine("After " + nCycles + " inverted cycles, we end up with the original card " + finalValueAtPosition + " at position " + cardToFollow);
			long originalCardToFollow = finalValueAtPosition;
			long finalPosition = PerformShuffle(originalActions, nCards, originalCardToFollow, nCycles);
			Console.WriteLine("After " + nCycles + ", original card " + originalCardToFollow + " is at " + finalPosition);*/
			if (loop == null) Console.WriteLine("NO LOOP FOUND!!!");
			else
			{
				Console.WriteLine("The loop length is " + loop.length + " and starts at " + loop.start);
				long nCyclesWithoutLoops = nCycles % loop.length;
				long finalValueWithoutLoops = PerformShuffle(invertedActions, nCards, cardToFollow, nCyclesWithoutLoops);
				long finalPositionWithoutLoops = PerformShuffle(originalActions, nCards, finalValueWithoutLoops, nCyclesWithoutLoops);
				Console.WriteLine("After " + nCyclesWithoutLoops + " cycles without loops, we have " + finalValueWithoutLoops + " at position " + finalPositionWithoutLoops);

				/*if (finalValueWithoutLoops != finalValueAtPosition || finalPosition != finalPositionWithoutLoops)
				{
					throw new Exception("Something is amiss!");
				}*/
			}

			Console.WriteLine("DONE!");
			Console.ReadKey();
		}

		private static Loop FindLoopLength(List<Action> actions, long nCards, long cardToFollow, long nTimes)
		{
			long originalCard = cardToFollow;
			//HashSet<long>[] prevPositionsPerAction = new HashSet<long>[actions.Count];
			//for (int i = 0; i < actions.Count; ++i) prevPositionsPerAction[i] = new HashSet<long>();
			//Dictionary<long, long> prevPositions = new Dictionary<long, long>();
			long nActions = 0;
			int nLoops = 0;
			long originalValue = cardToFollow;
			Dictionary<long, List<int>> firstCycle = new Dictionary<long, List<int>>();
			Console.WriteLine("Start at " + originalValue);
			for (long n = 0; n < nTimes; ++n)
			{
				if (n % 10000 == 0 && n > 0) Console.WriteLine("After " + n + " iterations...");
				for (int i = 0; i < actions.Count; ++i)
				{
					//var prevPositions = prevPositionsPerAction[i];
					Action action = actions[i];

					cardToFollow = ApplyAction(nCards, cardToFollow, action, i == actions.Count-1);

					// first cycle - remember all positions
					if (n == 0)
					{
						if (!firstCycle.ContainsKey(cardToFollow)) firstCycle.Add(cardToFollow, new List<int>());
						firstCycle[cardToFollow].Add(i);
					}

					else if (firstCycle.ContainsKey(cardToFollow))
					{
						var allPos = firstCycle[cardToFollow];
						for (int posIndex = 0; posIndex < allPos.Count; ++posIndex)
						{
							long loopStart = allPos[posIndex];
							long loopStop = nActions;
							long loopLength = loopStop - loopStart + 1;
							if (loopLength >= actions.Count && loopLength % actions.Count == 0)
							{
								Console.WriteLine("Loop starts at " + loopStart + " and stops at " + loopStop + ", length " + loopLength + "!");
								++nLoops;
								if (nLoops > 0) return new Loop() { start = loopStart, length = loopLength };
							}
						}
					}

					++nActions;
				}
			}

			return null;
		}

		private static long ApplyAction(long nCards, long cardToFollow, Action action, bool lastAction)
		{
			if (action.technique == Technique.DealIntoNewStack) cardToFollow = DealIntoNewStack(nCards, cardToFollow);
			else if (action.technique == Technique.Cut) cardToFollow = Cut(nCards, action.value, action.multiplier, cardToFollow);
			else cardToFollow = DealWithIncrement(nCards, action.value, action.offset, action.multiplier, cardToFollow);
			if (lastAction && action.multiplier < 0) cardToFollow = nCards - 1 - cardToFollow;
			return cardToFollow;
		}

		private static long PerformShuffle(List<Action> actions, long nCards, long cardToFollow, long nTimes)
		{
			long originalCard = cardToFollow;
			HashSet<long> offsets = new HashSet<long>();
			for (long n = 0; n < nTimes; ++n)
			{
				if (n % 10000 == 0) Console.WriteLine("After " + n + " iterations...");
				for (int i = 0; i < actions.Count; ++i)
				{
					Action action = actions[i];
					cardToFollow = ApplyAction(nCards, cardToFollow, action, i == actions.Count-1);
				}
			}

			//Console.WriteLine("Card " + originalCard + " is at " + cardToFollow);

			return cardToFollow;
		}

		static long DealIntoNewStack(long cardCount, long cardToFollow)
		{
			// invert
			return cardCount - 1 - cardToFollow;
		}

		static long Cut(long cardCount, int value, int multiplier, long cardToFollow)
		{
			// shift!
			if (multiplier < 0) value = -value;
			return (cardToFollow - value + cardCount) % cardCount;
		}

		static long DealWithIncrement(long cardCount, int n, long offset, int multiplier, long cardToFollow)
		{
			// if there is a multiplier, we invert the cardToFollow
			if (multiplier < 0) cardToFollow = cardCount - 1 - cardToFollow;

			// ??
			if (n > 0)
			{
				cardToFollow -= offset;
				if (cardToFollow < 0) cardToFollow = cardCount + cardToFollow;
				cardToFollow = (n * cardToFollow + cardCount) % cardCount;
			}
			else
			{
				// THIS IS WHAT SHOULD HAPPEN WHEN THE MULTIPLIER IS -1:
				/*cardToFollow = cardCount - 1 - cardToFollow;
				cardToFollow = (Math.Abs(n) * cardToFollow + cardCount) % cardCount;
				cardToFollow = cardCount - 1 - cardToFollow;
				return cardToFollow;*/

				// THIS IS WHAT SHOULD HAPPEN WHEN REVERTING THE ORDER OF OPERATIONS:
				cardToFollow -= offset;
				if (cardToFollow < 0) cardToFollow = cardCount + cardToFollow;
				while (cardToFollow % n != 0) cardToFollow += cardCount;
				if (cardToFollow < 0) throw new Exception("Should not happen.");
				cardToFollow = cardToFollow / Math.Abs(n);
				return cardToFollow;
			}

			if (multiplier < 0) cardToFollow = cardCount - 1 - cardToFollow;

			return cardToFollow;
		}

		static long GetNewStackLoopLength(long cardCount)
		{
			return 1;
			return 2;
		}

		static long GetCutLoopLength(long cardCount, int value)
		{
			return 1;
			int abs = Math.Abs(value);
			long multiple = cardCount;
			while (multiple % abs != 0) multiple += cardCount;
			return multiple / abs;
		}
		
		static long GetIncrementLoopLength(long cardCount, int n)
		{
			int val = Math.Abs(n);
			long pow = val;
			long nLoops = 1;
			while (pow != 1)
			{
				pow = (pow * val) % cardCount;
				++nLoops;
			}
			Console.WriteLine("Loop length for " + cardCount + " incr " + n + " is " + nLoops);
			return nLoops;
		}

		// 10461
		// -9
		static long GetIncrementLoopLengthForCard(long cardCount, long cardToFollow, int n)
		{
			int val = Math.Abs(n);
			long originalCard = cardToFollow;
			long pow = val;
			long nLoops = 1;
			cardToFollow = DealWithIncrement(cardCount, n, 0, 1, cardToFollow);
			while (originalCard != cardToFollow)
			{
				cardToFollow = DealWithIncrement(cardCount, n, 0, 1, cardToFollow);
				++nLoops;
			}
			Console.WriteLine("Loop length for " + cardCount + " incr " + n + " is " + nLoops);
			return nLoops;
		}

	}
}
