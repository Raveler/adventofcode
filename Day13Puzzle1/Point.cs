﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

struct Point : IEquatable<Point>
{
	public long x;
	public long y;

	public Point(long x, long y)
	{
		this.x = x;
		this.y = y;
	}

	public bool Equals(Point other)
	{
		return x == other.x && y == other.y;
	}

	public override int GetHashCode()
	{
		return (x, y).GetHashCode();
	}

	public override string ToString()
	{
		return "(" + x + "," + y + ")";
	}
}