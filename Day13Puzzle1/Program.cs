﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Day13Puzzle1
{
	enum TileType {
		Empty = 0,
		Wall = 1,
		Block = 2,
		HorizontalPaddle = 3,
		Ball = 4,
	}

	class Tile
	{

		public Point loc;
		public TileType type;
	}

	class Program
	{
		static Point ballLoc, paddleLoc;
		static Dictionary<Point, Tile> tiles = new Dictionary<Point, Tile>();
		static Point min, max;
		static int nBlockTiles = 0;
		static IntcodeComputer generator;
		static long score = -1;

		static void Main(string[] args)
		{

			string text = File.ReadAllText("input.txt");
			long[] program = text.Split(',').Select(x => long.Parse(x)).ToArray();
			generator = new IntcodeComputer(program, new long[0]);

			// insert coins
			generator.SetMemoryAddress(0, 2);

			while (true)
			{
				bool done = UpdateTile();
				if (done) break;
			}

			Console.WriteLine("Setup done!");
			Console.WriteLine("There are " + nBlockTiles + " block tiles");

			// start playing!!!
			long score = 0;
			min = new Point(tiles.Values.Min(x => x.loc.x), tiles.Values.Min(x => x.loc.y));
			max = new Point(tiles.Values.Max(x => x.loc.x), tiles.Values.Max(x => x.loc.y));
			Console.WriteLine("Domain goes from " + min + " to " + max);
			//Render();
			while (nBlockTiles > 0 && !generator.IsDone())
			{
				// always move the paddle into the direction of the ball
				long input = 0;
				if (ballLoc.x < paddleLoc.x) input = -1;
				else if (ballLoc.x > paddleLoc.x) input = 1;
				generator.AddInput(input);

				while (true)
				{
					bool done = UpdateTile();
					//Render();
					if (done) break;
					//Thread.Sleep(50);
				}

			}

			Render();

			Console.ReadKey();


		}

		private static bool UpdateTile()
		{
			IntCodeResponse response = generator.CalculateProgram();
			if (response.type == IntCodeResponseType.Done) return true;

			// the game is loaded! start playing!
			if (response.type == IntCodeResponseType.InputNeeded) return true;
			long x = response.value;
			long y = generator.CalculateProgram().value;
			long tileId = generator.CalculateProgram().value;

			// score!
			if (x == -1 && y == 0)
			{
				score = tileId;
				return false;
			}

			Point loc = new Point(x, y);
			Tile tile = new Tile()
			{
				loc = loc,
				type = (TileType)tileId,
			};

			// tile already exists!!! see if it was a block and is not anymore
			if (tiles.ContainsKey(loc))
			{
				if (tiles[loc].type == TileType.Block && tile.type != TileType.Block) --nBlockTiles;
				if (tiles[loc].type != TileType.Block && tile.type == TileType.Block) ++nBlockTiles;
				tiles[loc] = tile;
			}
			else
			{
				tiles.Add(loc, tile);
				if (tile.type == TileType.Block) ++nBlockTiles;
			}

			if (tile.type == TileType.Ball) ballLoc = loc;
			if (tile.type == TileType.HorizontalPaddle) paddleLoc = loc;

			return false;
		}

		private static void Render()
		{
			Console.Clear();
			StringBuilder ss = new StringBuilder();
			for (long y = min.y; y <= max.y; ++y)
			{
				for (long x = min.x; x <= max.x; ++x)
				{
					Console.BackgroundColor = ConsoleColor.Gray;
					char c = ' ';
					Point loc = new Point(x, y);
					if (tiles.ContainsKey(loc)) {
						Tile tile = tiles[loc];
						switch (tile.type) {
							case TileType.Ball: c = 'O'; Console.BackgroundColor = ConsoleColor.Red;  break;
							case TileType.Block: c = '#'; Console.BackgroundColor = ConsoleColor.DarkGray;  break;
							case TileType.Empty: c = ' '; break;
							case TileType.HorizontalPaddle: c = '-'; Console.BackgroundColor = ConsoleColor.Cyan;  break;
							case TileType.Wall: c = 'W'; Console.BackgroundColor = ConsoleColor.Black;  break;

						}
					}
					ss.Append(c);
					//Console.Write(c);
				}
				ss.AppendLine();
				//Console.WriteLine();
			}
			ss.AppendLine("Score: " + score);
			Console.Write(ss.ToString());
			
		}
	}
}
