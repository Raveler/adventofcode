﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day21Puzzle1
{
	class Program
	{
		static void Main(string[] args)
		{
			long[] program = IntcodeComputer.ParseProgram("input.txt");
			IntcodeComputer computer = new IntcodeComputer(program, new long[0]);

			var response = computer.CalculateProgram();
			while (response.type != IntCodeResponseType.InputNeeded)
			{
				if (response.type == IntCodeResponseType.Output) Console.Write((char)response.value);
				response = computer.CalculateProgram();
			}

			string[] springscript = File.ReadAllLines("springscript.txt");
			foreach (string line in springscript)
			{
				foreach (char c in line) {
					computer.AddInput((long)c);
				}
				computer.AddInput(10);
			}

			int outputLines = 0;
			bool outputLining = false;
			int[] layout = new int[50];
			int layoutIndex = 0;
			int jumpIndex = 0;
			while (!computer.IsDone())
			{
				var result = computer.CalculateProgram();
				if (result.type == IntCodeResponseType.Output)
				{
					char c = (char)result.value;
					Console.Write((char)result.value);

					if (c == '#' && !outputLining)
					{
						++outputLines;
						outputLining = true;
						layoutIndex = 0;
					}
					if ((int)c == 10 && outputLining)
					{
						outputLining = false;
					}
					if (outputLining) {
						if (c == '@') jumpIndex = layoutIndex;
						layout[layoutIndex] = (c == '#' ? 1 : 0);
						++layoutIndex;
					}

					if (result.value > 255) Console.WriteLine("Hull damage is " + result.value);
				}
				else
				{
					Console.WriteLine("Found " + result.type);
				}
			}

			if (outputLines > 0)
			{
				for (int i = 0; i < 9; ++i)
				{
					Console.Write((char)((int)'A' + i));
				}
				Console.WriteLine();
				for (int i = 0; i < 9; ++i)
				{
					int relativeIndex = jumpIndex - 5 + i;
					Console.Write(layout[relativeIndex]);
				}
				Console.WriteLine();
			}

			Console.WriteLine("DONE");
			Console.ReadKey();
		}
	}
}
