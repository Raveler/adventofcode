﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day14Puzzle1
{
	public class Formula {
		public string[] inputs;
		public long[] amounts;
		public long output;
		public bool raw;
	}

	public class Element {
		public string name;
		public Formula formula;
	}

	public class RawElement {
		public string name;
		public long consumed = 0;
		public long produced = 0;
	}

	class Program
	{
		private static Dictionary<string, Element> elements = new Dictionary<string, Element>();
		private static Formula fuelFormula;


		static void Main(string[] args)
		{
			string[] lines = File.ReadAllLines("input.txt");
			Regex regexp = new Regex("(([0-9]+) ([A-Z]+),? )+=> (([0-9]+) ([A-Z]+))");
			for (int i = 0; i < lines.Length; ++i)
			{
				string line = lines[i];
				var match = regexp.Match(line);

				int nInputs = (match.Groups[1].Captures.Count);

				List<string> inputs = new List<string>();
				List<long> amounts = new List<long>();
				for (int j = 0; j < nInputs; ++j) {
					var captures = match.Groups[3].Captures;
					inputs.Add(match.Groups[3].Captures[j].Value);
					amounts.Add(int.Parse(match.Groups[2].Captures[j].Value));
				}

				string elementName = match.Groups[6].Value;
				Formula formula = new Formula()
				{
					inputs = inputs.ToArray(),
					amounts = amounts.ToArray(),
					output = int.Parse(match.Groups[5].Value),
					raw = (inputs[0] == "ORE"),
				};

				elements.Add(elementName, new Element()
				{
					formula = formula,
					name = elementName,
				});

				if (elementName == "FUEL") fuelFormula = formula;
			}

			// total ore
			long maxOre = 1000000000000;

			long totalOre = ProduceFuel(1);
			Console.WriteLine("TOTAL ORE FOR ONE: " + totalOre);

			long fuelEstimation = maxOre / totalOre;
			Console.WriteLine("TOTAL ORE FOR " + fuelEstimation + ": " + ProduceFuel(fuelEstimation));

			// increase until we are beyond maxOre
			bool expanding = true;
			long multiplier = 1;
			long min = fuelEstimation;
			while (expanding)
			{
				fuelEstimation += multiplier;
				multiplier *= 2;
				long ore = ProduceFuel(fuelEstimation);
				expanding = ore < maxOre;
			}

			long max = fuelEstimation;
			Console.WriteLine("Search between " + min + " (" + ProduceFuel(min) + ") and " + max + " (" + ProduceFuel(max) + ")");
			long best = BinarySearch(min, max, maxOre);
			Console.WriteLine("TOTAL ORE FOR " + best + " fuel: " + ProduceFuel(best));
			
			Console.ReadKey();

		}

		private static long BinarySearch(long min, long max, long maxOre, long it = 0)
		{
			if (min == max) return min;
			if (it == 500) return -1;
			if (min == max - 1)
			{
				Console.WriteLine("Uh-oh!");
				long more = ProduceFuel(max);
				Console.WriteLine(more);
				Console.WriteLine(ProduceFuel(max+1));
			}
			long middle = (max + min) / 2;
			long ore = ProduceFuel(middle);
			Console.WriteLine("Look between " + min + " and " + max + ", evaluate " + middle + " to " + ore);
			if (ore > maxOre)
			{
				return BinarySearch(min, middle - 1, maxOre, it+1);
			}
			else
			{
				return BinarySearch(middle, max, maxOre, it+1);
			}
		}

		private static long ProduceFuel(long fuelCount)
		{
			long totalOre = 0;

			Dictionary<string, RawElement> rawElements = new Dictionary<string, RawElement>();
			List<Tuple<string, long>> requiredElements = new List<Tuple<string, long>>();
			Dictionary<string, long> producedElements = new Dictionary<string, long>();

			// start cooking!
			requiredElements.Add(new Tuple<string, long>("FUEL", fuelCount));
			while (requiredElements.Count > 0)
			{
				Tuple<string, long> nextElement = requiredElements[0];
				requiredElements.RemoveAt(0);
				string name = nextElement.Item1;
				long amount = nextElement.Item2;

				// look up the formula
				Formula formula = elements[name].formula;

				// we don't need the formula - we already produced it before
				if (producedElements.ContainsKey(name) && producedElements[name] > 0)
				{
					//Console.WriteLine("Amount " + producedElements[name] + " " + name + " already produced, so use it");
					long nAvailable = producedElements[name];
					long nConsumed = Math.Min(nAvailable, amount);
					producedElements[name] -= nConsumed;
					amount -= nConsumed;

					if (!rawElements.ContainsKey(name)) rawElements[name] = new RawElement() { name = name };
					rawElements[name].consumed += nConsumed;
				}

				// multiply the formula by an amount so that we get the right amount of stuff
				long multiplier = (long)Math.Ceiling((double)amount / (double)formula.output);
				//Console.WriteLine("Request " + amount + " " + name + ", formula multiplier " + multiplier + " so we will produce " + (formula.output * multiplier) + " elements");

				// raw!
				if (formula.raw && amount > 0)
				{
					if (!rawElements.ContainsKey(name)) rawElements[name] = new RawElement() { name = name };
					rawElements[name].produced += formula.output * multiplier;
					rawElements[name].consumed += amount;
				}


				// we execute the formula only when we need to
				if (amount > 0)
				{

					// now add the requested elemenets to the list
					for (int i = 0; i < formula.inputs.Length; ++i)
					{
						string requestedName = formula.inputs[i];
						long requestedAmount = formula.amounts[i] * multiplier;

						// this is ore
						if (requestedName == "ORE")
						{
							//Console.WriteLine("Requires " + requestedAmount + " ORE");
							totalOre += requestedAmount;
							continue; // all done
						}

						// we already produced this before (from another formula probably)
						else if (producedElements.ContainsKey(name) && producedElements[name] > 0)
						{
							long nAvailable = producedElements[name];
							long nConsumed = Math.Min(nAvailable, requestedAmount);
							//Console.WriteLine("Requested amount " + requestedAmount + " " + requestedName + " already produced, so use it");
							producedElements[name] -= nConsumed;
							requestedAmount -= nConsumed;
						}

						// otherwise, request it now
						if (requestedAmount > 0)
						{
							//Console.WriteLine("We need new production of " + requestedAmount + " " + requestedName + "!");
							requiredElements.Add(new Tuple<string, long>(requestedName, requestedAmount));
						}
					}

					// produced!
					long producedAmount = formula.output * multiplier;
					long surplus = producedAmount - amount;
					if (!producedElements.ContainsKey(name)) producedElements[name] = 0;
					producedElements[name] += surplus;
				}

			}

			foreach (string name in rawElements.Keys)
			{
				//Console.WriteLine("raw element " + name + " was produced " + rawElements[name].produced + " times, surplus " + (rawElements[name].produced - rawElements[name].consumed));
			}
			/*
			int nFuels = 0;
			totalOre = 0;
			RawElement[] rawElementArr = rawElements.Values.ToArray();
			long totalSurplus = 0;
			long totalOreNeeded = 0;
			for (int i = 0; i < rawElementArr.Length; ++i)
			{
				totalOreNeeded += rawElementArr[i].produced;
				totalSurplus += rawElementArr[i].produced - rawElementArr[i].consumed;
			}
			int surplusFixesStuff = totalOreNeeded / totalSurplus;
			*/
			return totalOre;
		}
	}
}
