﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11Puzzle1
{
	class Program
	{
		private static Dictionary<Point, int> panels = new Dictionary<Point, int>();

		static void Main(string[] args)
		{

			string text = File.ReadAllText("input.txt");
			long[] program = text.Split(',').Select(x => long.Parse(x)).ToArray();

			panels[new Point(0, 0)] = 1;

			IntcodeComputer computer = new IntcodeComputer(program, new long[0]);

			// keep going until the program is done
			Point p = new Point() { x = 0, y = 0 };
			Point dir = new Point() { x = 0, y = 1 };
			while (!computer.IsDone())
			{
				// give it as input the current color
				int color = GetColor(p);
				computer.AddInput(color);

				// now wait for 2 outputs - the color and turn signal
				int newColor = (int)computer.CalculateProgram();
				int turnSignal = (int)computer.CalculateProgram();
				Console.WriteLine("newColor is " + newColor + ", turnSignal is " + turnSignal);

				if (computer.IsDone()) break;

				// apply the new color
				panels[p] = newColor;

				// turn & move
				if (turnSignal == 0) dir = new Point() { x = -dir.y, y = dir.x };
				else if (turnSignal == 1) dir = new Point() { x = dir.y, y = -dir.x };
				else throw new Exception("ERROR ERROR");
				p = new Point() { x = p.x + dir.x, y = p.y + dir.y };
			}

			// get the minimum and maximum coordinates
			Point min = new Point(panels.Keys.Select(pp => pp.x).Min(), panels.Keys.Select(pp => pp.y).Min());
			Point max = new Point(panels.Keys.Select(pp => pp.x).Max(), panels.Keys.Select(pp => pp.y).Max());

			for (int y = max.y; y >= min.y; --y)
			{
				for (int x = min.x; x <= max.x; ++x)
				{
					int color = GetColor(new Point(x, y));
					Console.BackgroundColor = (color == 1 ? ConsoleColor.White : ConsoleColor.Black);
					Console.Write(" ");
				}
				Console.BackgroundColor = ConsoleColor.Black;
				Console.WriteLine();
			}


			int nPainted = panels.Count;
			Console.WriteLine("We painted " + nPainted + " panels");

			Console.ReadKey();
		}

		static int GetColor(Point p)
		{
			if (!panels.ContainsKey(p)) return 0;
			return panels[p];
		}
	}
}
