﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

struct Point : IEquatable<Point>
{
	public long x;
	public long y;
	public long z;

	public Point(long x, long y, long z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public bool Equals(Point other)
	{
		return x == other.x && y == other.y && z == other.z;
	}

	public override int GetHashCode()
	{
		return (x, y, z).GetHashCode();
	}
}