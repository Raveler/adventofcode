﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day12Puzzle1
{
	class Moon
	{
		public Point position;
		public Point velocity;
		public Point originalPosition;
	}



	class Program
	{
		static Moon[] moons;

		static readonly Point Zero = new Point(0, 0, 0);


		static void Main(string[] args)
		{

			// parse the input
			string[] lines = File.ReadAllLines("input.txt");
			Regex regexp = new Regex("<x=([-0-9]+), y=([-0-9]+), z=([-0-9]+)>");
			moons = new Moon[lines.Length];
			for (int i = 0; i < lines.Length; ++i)
			{
				Match match = regexp.Match(lines[i]);
				int x = int.Parse(match.Groups[1].Value);
				int y = int.Parse(match.Groups[2].Value);
				int z = int.Parse(match.Groups[3].Value);
				Moon moon = new Moon()
				{
					position = new Point(x, y, z),
					velocity = new Point(0, 0, 0),
					originalPosition = new Point(x, y, z),
				};
				moons[i] = moon;
			}

			// now do n steps
			for (int i = 0; true; ++i)
			{
				if (Simulate(i)) break;
			}

			// count the energy
			long energy = 0;
			foreach (Moon moon in moons)
			{
				long pot = Math.Abs(moon.position.x) + Math.Abs(moon.position.y) + Math.Abs(moon.position.z);
				long kin = Math.Abs(moon.velocity.x) + Math.Abs(moon.velocity.y) + Math.Abs(moon.velocity.z);
				energy += pot * kin;
			}

			Console.WriteLine("ENERGY: " + energy);
			Console.ReadKey();
		}

		static long lastXCycle = -1, lastYCycle = -1, lastZCycle = -1;
		static long XCycle = 0, YCycle = 0, ZCycle = 0;

		static bool Simulate(long iteration)
		{
			if (iteration % 1000000 == 0) Console.WriteLine("SIMULATE " + iteration);

			// gravity
			foreach (Moon moon in moons)
			{
				ApplyGravity(moon);
			}

			// velocity
			long sumPos = 0, sumVel = 0;
			int xCycles = 0, yCycles = 0, zCycles = 0;
			for (int i = 0; i < moons.Length; ++i)
			{
				Moon moon = moons[i];

				moon.position.x += moon.velocity.x;
				moon.position.y += moon.velocity.y;
				moon.position.z += moon.velocity.z;
				sumPos += moon.position.x + moon.position.y + moon.position.z;
				sumVel += moon.velocity.x + moon.velocity.y + moon.velocity.z;

				if (moon.position.x == moon.originalPosition.x && moon.velocity.x == 0) ++ xCycles;
				if (moon.position.y == moon.originalPosition.y && moon.velocity.y == 0) ++ yCycles;
				if (moon.position.z == moon.originalPosition.z && moon.velocity.z == 0) ++ zCycles;
			}

			if (xCycles == moons.Length) {
				XCycle = (iteration - lastXCycle);
				Console.WriteLine("X in sync after " + XCycle + " steps");
				lastXCycle = iteration;
			}
			if (yCycles == moons.Length) {
				YCycle = (iteration - lastYCycle);
				Console.WriteLine("Y in sync after " + YCycle + " steps");
				lastYCycle = iteration;
			}
			if (zCycles == moons.Length) {
				ZCycle = (iteration - lastZCycle);
				Console.WriteLine("Z in sync after " + ZCycle + " steps");
				lastZCycle = iteration;
			}

			if (lastXCycle != -1 && lastYCycle != -1 && lastZCycle != -1)
			{
				CalculateFullCycle();
				return true;
			}

			return false;

			//Console.WriteLine("SumPos: " + sumPos + ", sumVel: " + sumVel);
		}

		static void CalculateFullCycle()
		{
			// find the least common multiple of all the cycles
			List<long> numbers = new List<long>(new long[] { XCycle, YCycle, ZCycle });
			Console.WriteLine("CALCULATE FULL CYCLE FOR " + XCycle + "," + YCycle + "," + ZCycle);
			numbers.Sort();
			long largest = numbers[numbers.Count - 1];
			numbers.RemoveAt(numbers.Count - 1);

			bool found = false;
			long multiple = largest;
			while (!found)
			{
				if (multiple % numbers[0] == 0 && multiple % numbers[1] == 0)
				{
					found = true;
					break;
				}
				multiple += largest;
			}

			Console.WriteLine("FOUND MULTIPLE: " + multiple);

		}

		static void ApplyGravity(Moon moon)
		{
			for (int i = 0; i < moons.Length; ++i)
			{
				Moon otherMoon = moons[i];
				if (moon == otherMoon) continue;
				int dx = Math.Sign(otherMoon.position.x - moon.position.x);
				int dy = Math.Sign(otherMoon.position.y - moon.position.y);
				int dz = Math.Sign(otherMoon.position.z - moon.position.z);
				moon.velocity.x += dx;
				moon.velocity.y += dy;
				moon.velocity.z += dz;
			}
		}
	}
}
