﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day17Puzzle1
{
	/*enum TileType {
		Scaffold = 35,
		Air = 46,
		RobotUp,*/

	class Tile
	{
		public Point loc;
		public long tileType;
	}

	class Step
	{
		public char instruction;
		public int amount;
	}

	class Path
	{
		public List<Step> steps = new List<Step>();
	}


	class Program
	{
		static Dictionary<Point, Tile> tiles = new Dictionary<Point, Tile>();
		const int WallTile = 35;
		static IntcodeComputer robot;

		static void Main(string[] args)
		{

			string text = File.ReadAllText("input.txt");
			long[] program = text.Split(',').Select(v => long.Parse(v)).ToArray();
			program[0] = 2; // override mode!
			robot =  new IntcodeComputer(program, new long[0]);

			int x = 0, y = 0;
			int width = 0;
			while (!robot.IsDone())
			{
				var response = robot.CalculateProgram();
				if (response.type != IntCodeResponseType.Done)
				{
					if (response.type == IntCodeResponseType.InputNeeded) break;
					long output = response.value;

					if (output == 10)
					{
						++y;
						//Console.WriteLine("Newline at " + x + "," + y);
						if (x > width) width = x;
						x = 0;
					}
					else
					{
						var loc = new Point(x, y);
						Tile tile = new Tile() { loc = loc, tileType = output };
						tiles.Add(loc, tile);
						++x;
					}
				}
			}
			int height = y - 2;

			DrawMap(width, height);

			Dictionary<Point, Tile> correctTiles = new Dictionary<Point, Tile>();
			for (y = 0; y < height; ++y)
			{
				for (x = 0; x < width; ++x)
				{
					Tile tile = tiles[new Point(x, y)];
					Point newLoc = new Point(x, height - 1 - y);
					tile.loc = newLoc;
					correctTiles.Add(newLoc, tile);
				}
			}
			tiles = correctTiles;

			//LoadMap();



			// convert the tiles into a path
			Tile robotTile = tiles.Values.Where(r => r.tileType == '^').FirstOrDefault();

			// do a walk
			bool finished = false;
			Point lookDir = new Point(0, 1);
			Path path = new Path();
			Point prevTile = new Point(-1, 0);
			Console.WriteLine("Start at " + robotTile.loc);
			while (!finished)
			{
				// find the direction of the path
				Tile nextTile = FindNextStep(prevTile, robotTile);
				if (nextTile == null)
				{
					Console.WriteLine("End of the line!");
					Console.WriteLine("END AT " + robotTile.loc + "!");
					break;
				}
				//Console.WriteLine("nextTile is " + nextTile.loc + " from robotTile " + robotTile.loc + " and prevTile " + prevTile);

				// turn in the right dir
				Point dir = new Point(nextTile.loc.x - robotTile.loc.x, nextTile.loc.y - robotTile.loc.y);
				//Console.WriteLine("Dir is " + dir + ", lookDir is " + lookDir);
				if (lookDir.x == -dir.y && lookDir.y == dir.x)
				{
					path.steps.Add(new Step() { instruction = 'R', amount = 0 });
					//Console.WriteLine("R");
				}
				else
				{
					path.steps.Add(new Step() { instruction = 'L', amount = 0 });
					//Console.WriteLine("L");
				}

				// now move forward as far as we can
				int nSteps = 0;
				Point nextLoc = nextTile.loc;
				while (true)
				{
					nextTile = tiles[nextLoc];
					++nSteps;
					nextLoc = new Point(nextLoc.x + dir.x, nextLoc.y + dir.y);
					if (!tiles.ContainsKey(nextLoc)) break;
					if (tiles[nextLoc].tileType != WallTile) break;
				}
				robotTile = nextTile;
				prevTile = new Point(robotTile.loc.x - dir.x, robotTile.loc.y - dir.y);
				path.steps.Add(new Step() { instruction = 'M', amount = nSteps });
				lookDir = dir;
				//Console.WriteLine(nSteps + ", now at " + nextTile.loc);
			}

			string pathStr = "";
			for (int i = 0; i < path.steps.Count; ++i)
			{
				var step = path.steps[i];
				if (step.instruction == 'M')
				{
					Console.Write(step.amount + ",");
					pathStr += step.amount + ",";
				}
				else
				{
					Console.Write(step.instruction + ",");
					pathStr += step.instruction + ",";
				}
			}
			//pathStr = pathStr.Substring(0, pathStr.Length);
			Console.WriteLine();
			Console.WriteLine("Full length: " + pathStr.Length);

			//string A = "R,8,L,10,R,8,R,12,R,8,L,8";
			/*string A = "R,8,L,10,R,8,R,12,R";
			string B = "8,L,8";
			pathStr = pathStr.Replace(A, "[" + A + "]");
			pathStr = pathStr.Replace(B, "[" + B + "]");
			Console.WriteLine("LEFT: " + pathStr);*/

			// generate the right set
			int nMaxSets = 3;
			string[] currentSubsets = new string[3];
			//Bingo bingo = FindBestSubsets(pathStr, currentSubsets, 0, nMaxSets, 20, 0, 10);
			Bingo bingo = new Bingo() {
				replacementPath = "A#C#A#B#A#C#B#B#A#C#",
				//replacementPath = "A#",
				subsets = new string[] { "R,8,L,10,R,8,", "L,12,L,10,L,8,", "R,12,R,8,L,8,L,12,"}
			};
			Console.WriteLine("WHOOOOOO");

			IntCodeResponse answer;
			while ((answer = robot.CalculateProgram()).type != IntCodeResponseType.InputNeeded);

			// feed it test instructions
			string mainRoutine = bingo.replacementPath.Replace("#", ",");
			robot.AddLine(mainRoutine.Substring(0, mainRoutine.Length-1));
			while ((answer = robot.CalculateProgram()).type != IntCodeResponseType.InputNeeded) ;
			//robot.AddLine("R,8,L,10,R,8,R,2,R,3");
			robot.AddLine(bingo.subsets[0].Substring(0, bingo.subsets[0].Length-1));
			while ((answer = robot.CalculateProgram()).type != IntCodeResponseType.InputNeeded) ;
			robot.AddLine(bingo.subsets[1].Substring(0, bingo.subsets[1].Length-1));
			while ((answer = robot.CalculateProgram()).type != IntCodeResponseType.InputNeeded) ;
			robot.AddLine(bingo.subsets[2].Substring(0, bingo.subsets[2].Length-1));
			while ((answer = robot.CalculateProgram()).type != IntCodeResponseType.InputNeeded) ;
			robot.AddLine("n");

			// execute the instructions
			while (answer.type != IntCodeResponseType.Done)
			{
				answer = robot.CalculateProgram();
				if (answer.type == IntCodeResponseType.Output)
				{
					var value = answer.value;
					if (value > 255) Console.WriteLine("EUH: " + value);
					else Console.Write((char)value);
				}

			}


			LoadMap();
			/*answer = robot.CalculateProgram();

			Console.WriteLine("DUST: " + answer.value);

			*/
			Console.WriteLine("DONE!");
			Console.ReadKey();
		}

		class Bingo
		{
			public string[] subsets;
			public string replacementPath;
		}

		static Bingo FindBestSubsets(string path, string[] currentSubsets, int subsetIndex, int maxSubsets, int maxLength, int currentReplaces, int maxReplaces)
		{
			List<string> exploredBefore = new List<string>();
			for (int length = 1; length <= maxLength; ++length)
			{
				for (int start = 0; start <= path.Length - length; ++start)
				{
					string substr = path.Substring(start, length);
					if (substr[0] == ',' || substr[substr.Length - 1] != ',') continue;
					if (substr.Contains('A') || substr.Contains('B') || substr.Contains('C')) continue;
					if (exploredBefore.Contains(substr)) continue;
					string newSubset = substr;
					exploredBefore.Add(newSubset);

					if (subsetIndex == 0) Console.WriteLine("Explore " + newSubset);
					currentSubsets[subsetIndex] = newSubset;

					// remove all occurences
					char rep = (char)((int)'A' + subsetIndex);
					string newPath = path.Replace(newSubset, rep + "#");
					//string newPath = path.Replace(newSubset, "");
					//string newReplacementPath = replacementPath.Replace(newSubset, rep + "#");

					// count the number of replaces
					int nReplaces = Regex.Matches(path, newSubset).Count;
					int newReplaces = currentReplaces + nReplaces;

					// we failed, too many replaces
					if (newReplaces > maxReplaces) continue;

					// DONE
					if (!newPath.Contains(","))
					{
						Console.WriteLine("FOUND MATCH WITH " + newReplaces + " REPLACES");
						Console.WriteLine("FINAL REPLACEMENT PATH: " + newPath);
						return new Bingo() { subsets = currentSubsets, replacementPath = newPath };
					}

					// we failed :(
					if (subsetIndex == maxSubsets - 1) continue;

					// we still have some left to go
					Bingo bestSubset = FindBestSubsets(newPath, currentSubsets, subsetIndex + 1, maxSubsets, maxLength, newReplaces, maxReplaces);
					if (bestSubset == null) continue;
					else return bestSubset;

					//subsets.Add(substr);
				}
			}

			/*List<string> newSubsets = GetSubsets(path, maxLength);
			foreach (string newSubset in newSubsets)
			{
				if (subsetIndex == 0) Console.WriteLine("Explore " + newSubset);
				currentSubsets[subsetIndex] = newSubset;

				// remove all occurences
				//string newPath = path.Replace(newSubset, "" + subsetIndex);
				char rep = (char)((int)'A' + subsetIndex);
				string newPath = path.Replace(newSubset, rep + "#");
				string newReplacementPath = replacementPath.Replace(newSubset, rep + "#");

				// count the number of replaces
				int nReplaces = Regex.Matches(path, newSubset).Count;
				int newReplaces = currentReplaces + nReplaces;

				// we failed, too many replaces
				if (newReplaces > maxReplaces) continue;

				// DONE
				if (!newPath.Contains(",")) {
					Console.WriteLine("FOUND MATCH WITH " + newReplaces + " REPLACES");
					Console.WriteLine("FINAL REPLACEMENT PATH: " + newReplacementPath);
					return new Bingo() { subsets = currentSubsets, replacementPath = newReplacementPath };
				}

				// we failed :(
				if (subsetIndex == maxSubsets - 1) continue;

				// we still have some left to go
				Bingo bestSubset = FindBestSubsets(newPath, newReplacementPath, currentSubsets, subsetIndex + 1, maxSubsets, maxLength, newReplaces, maxReplaces);
				if (bestSubset == null) continue;
				else return bestSubset;
			}*/
			return null;
		}

		static List<string> GetSubsets(string path, int maxLength)
		{
			List<string> subsets = new List<string>();
			for (int length = 1; length <= maxLength; ++length)
			{
				for (int start = 0; start <= path.Length - length; ++start)
				{
					string substr = path.Substring(start, length);
					if (substr[0] == ',' || substr[substr.Length - 1] != ',') continue;
					if (substr.Contains('A') || substr.Contains('B') || substr.Contains('C')) continue;
					subsets.Add(substr);
				}
			}
			return subsets;
		}

		static Tile FindNextStep(Point prevLoc, Tile curr)
		{
			Point loc = curr.loc;
			Point left = new Point(loc.x - 1, loc.y);
			Point right = new Point(loc.x + 1, loc.y);
			Point down = new Point(loc.x, loc.y - 1);
			Point up = new Point(loc.x, loc.y + 1);
			if (!left.Equals(prevLoc) && tiles.ContainsKey(left) && tiles[left].tileType == WallTile) return tiles[left];
			if (!right.Equals(prevLoc) && tiles.ContainsKey(right) && tiles[right].tileType == WallTile) return tiles[right];
			if (!down.Equals(prevLoc) && tiles.ContainsKey(down) && tiles[down].tileType == WallTile) return tiles[down];
			if (!up.Equals(prevLoc) && tiles.ContainsKey(up) && tiles[up].tileType == WallTile) return tiles[up];
			return null; // no path found!
		}

		static void LoadMap()
		{
			tiles.Clear();
			//Console.Clear();

			int x = 0, y = 0;
			int width = 0;
			while (!robot.IsDone())
			{
				var response = robot.CalculateProgram();
				if (response.type != IntCodeResponseType.Done)
				{
					if (response.type == IntCodeResponseType.InputNeeded) break;
					long output = response.value;

					if (output == 10)
					{
						++y;
						//Console.WriteLine("Newline at " + x + "," + y);
						if (x > width) width = x;
						x = 0;
					}
					else
					{
						var loc = new Point(x, y);
						Tile tile = new Tile() { loc = loc, tileType = output };
						tiles.Add(loc, tile);
						++x;
					}
				}
			}
			int height = y - 2;

			DrawMap(width, height);
		}

		private static void DrawMap(int width, int height)
		{
			// draw the map
			Console.WriteLine("Draw map of " + width + "," + height);
			int alignmentSum = 0;
			int nIntersections = 0;
			for (int y = 0; y < height; ++y)
			//for (y = height-1; y >= 0; --y)
			{
				for (int x = 0; x < width; ++x)
				{
					Point loc = new Point(x, y);
					if (!tiles.ContainsKey(loc))
					{
						Console.Write("?");
						continue;
					}

					Tile tile = tiles[loc];
					Console.Write((char)tile.tileType);

					// see if this is an intersection
					if (IsIntersection(loc))
					{
						alignmentSum += loc.x * loc.y;
						++nIntersections;
					}
				}
				Console.WriteLine();
			}

			Console.WriteLine("Found " + nIntersections + " intersections");
			Console.WriteLine("Alignment sum: " + alignmentSum);
		}

		static bool IsIntersection(Point loc)
		{
			Tile tile = tiles[loc];
			if (tile.tileType != WallTile) return false;

			// check all the neighbours
			if (!tiles.ContainsKey(new Point(loc.x - 1, loc.y))) return false;
			if (!tiles.ContainsKey(new Point(loc.x + 1, loc.y))) return false;
			if (!tiles.ContainsKey(new Point(loc.x, loc.y - 1))) return false;
			if (!tiles.ContainsKey(new Point(loc.x, loc.y + 1))) return false;

			// see if they're walls
			if (tiles[new Point(loc.x - 1, loc.y)].tileType != WallTile) return false;
			if (tiles[new Point(loc.x + 1, loc.y)].tileType != WallTile) return false;
			if (tiles[new Point(loc.x, loc.y - 1)].tileType != WallTile) return false;
			if (tiles[new Point(loc.x, loc.y + 1)].tileType != WallTile) return false;

			return true;

		}
	}
}
