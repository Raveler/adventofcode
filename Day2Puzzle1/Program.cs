﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2Puzzle1
{
	class Program
	{
		static void Main(string[] args)
		{
			string text = File.ReadAllText("input.txt");
			int[] codes = text.Split(',').Select(x => int.Parse(x)).ToArray();

			for (int noun = 0; noun <= 99; ++noun)
			{
				for (int verb = 0; verb <= 99; ++verb)
				{
					int val = CalculateProgram(codes, noun, verb);
					if (val == 19690720)
					{
						Console.WriteLine("Noun*100+Verb is " + (noun * 100 + verb));
						Console.ReadKey();
						return;
					}
				}
			}
		}

		static int CalculateProgram(int[] program, int noun, int verb)
		{
			int[] codes = program.ToArray();

			// gravity assist program
			codes[1] = noun;
			codes[2] = verb;

			int pc = 0;
			while (codes[pc] != 99) // halting code
			{
				int opcode = codes[pc];
				int op1Index = codes[pc + 1];
				int op2Index = codes[pc + 2];
				int outIndex = codes[pc + 3];

				int op1 = codes[op1Index];
				int op2 = codes[op2Index];
				int outValue;
				switch (opcode)
				{

					case 1:
						outValue = op1 + op2;
						break;

					case 2:
						outValue = op1 * op2;
						break;

					default: throw new Exception("ERROR ERROR");
				}

				codes[outIndex] = outValue;
				pc += 4;
			}

			return codes[0];
		}
	}
}
