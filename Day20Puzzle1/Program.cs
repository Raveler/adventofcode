﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20Puzzle1
{
	class Tile {
		public bool walkable;
		public Point loc;
		public string teleporter = null;
		public Point teleportsTo;
		public int distance = 0;
		public int level;
		public bool innerTeleporter;
		public Dictionary<Point, int> distanceTo = new Dictionary<Point, int>();
		public bool special;
	}

	class Program
	{
		static Dictionary<Point, Tile> template = new Dictionary<Point, Tile>();

		static int width, height;

		static void Main(string[] args)
		{
			string[] lines = File.ReadAllLines("input.txt");

			height = lines.Length - 4; // the surrounding tiles do not count
			width = lines[0].Length - 4;
			Console.WriteLine("width " + width + ", height " + height);
			Point start = new Point(0, 0);
			Point end = new Point(0, 0);
			for (int y = 0; y < height; ++y)
			{
				string line = lines[(height-1-y)+2];
				for (int x = 0; x < width; ++x)
				{
					Point loc = new Point(x, y);
					char value = line[x + 2];
					if (value != '.') continue;


					// teleports?
					string teleporter = GetTeleporter(lines, x, y);
					string realTeleporter = null;
					bool special = false;
					if (teleporter == "AA")
					{
						start = loc;
						special = true;
					}
					else if (teleporter == "ZZ")
					{
						end = loc;
						special = true;
					}
					else realTeleporter = teleporter;

					bool innerTeleporter = (0 < x && x < width - 1) && (0 < y && y < height - 1);

					Tile tile = new Tile()
					{
						loc = loc,
						walkable = true,
						teleporter = realTeleporter,
						level = 0,
						innerTeleporter = innerTeleporter,
						special = special,
					};

					template.Add(loc, tile);
				}
			}


			// link the teleporters
			foreach (Tile tile in template.Values)
			{
				if (tile.teleporter != null)
				{
					tile.teleportsTo = template.Values.Where(x => x.teleporter == tile.teleporter && x != tile).First().loc;
				}
			}
			Console.WriteLine("There are " + template.Values.Count(x => x.innerTeleporter && x.teleporter != null) + " inner and " + template.Values.Count(x => !x.innerTeleporter && x.teleporter != null) + " outer teleporters");

			// connect nodes
			foreach (Tile tile in template.Values)
			{
				if (tile.special || tile.teleporter != null)
				{
					FindNeighbours(template, tile.loc);
				}
			}
			Console.WriteLine("Linked neighbours...");

			int steps = Solve(template, start, end);

			Console.WriteLine("Solved in " + steps + " steps");

			Console.WriteLine("DONE");
			Console.ReadKey();
		}

		static void FindNeighbours(Dictionary<Point, Tile> template, Point loc)
		{
			Tile start = template[loc];

			foreach (Tile tile in template.Values) {
				tile.distance = int.MaxValue;
			}

			// do a dijkstra to figure out the fastest way to all available points
			List<Tile> open = template.Values.ToList();
			start.distance = 0;
			while (open.Count > 0)
			{
				open.Sort(CompareTile);
				Tile next = open[0];

				// couldn't reach this tile!
				if (next.distance == int.MaxValue) break;

				open.RemoveAt(0);
				//Console.WriteLine("Tile " + next.loc + " is at distance " + next.distance);

				if (next.teleporter != null || next.special)
				{
					if (next.distance != int.MaxValue && next != start)
					{
						start.distanceTo[next.loc] = next.distance;
					}
				}

				// don't pass through a key
				int distance = next.distance + 1;
				foreach (Point neighbourLoc in next.loc.GetNeighbours())
				{
					if (!template.ContainsKey(neighbourLoc)) continue;
					Tile neighbour = template[neighbourLoc];
					if (distance < neighbour.distance) neighbour.distance = distance;

				}
			}
		}

		static Dictionary<Point, Tile> MakeCopy(Dictionary<Point, Tile> template, int level)
		{
			Console.WriteLine("Make copy for level " + level);
			Dictionary<Point, Tile> copy = new Dictionary<Point, Tile>();
			foreach (Point key in template.Keys)
			{
				Tile tile = template[key];
				if (!tile.special && tile.teleporter == null) continue;
				copy.Add(key, new Tile()
				{
					loc = tile.loc,
					teleporter = tile.teleporter,
					teleportsTo = tile.teleportsTo,
					level = level,
					distance = int.MaxValue,
					innerTeleporter = tile.innerTeleporter,
					special = tile.special,
					distanceTo = new Dictionary<Point, int>(tile.distanceTo),
				});
			}
			return copy;
		}

		static int Solve(Dictionary<Point, Tile> template, Point startLoc, Point endLoc)
		{
			Dictionary<int, Dictionary<Point, Tile>> levels = new Dictionary<int, Dictionary<Point, Tile>>();
			levels[0] = MakeCopy(template, 0);

			// do a dijkstra to figure out the fastest way to all available points
			List<Tile> open = levels[0].Values.Where(x => x.special || x.teleporter != null).ToList();
			Tile start = levels[0][startLoc];
			levels[0][startLoc].distance = 0;
			while (open.Count > 0)
			{
				open.Sort(CompareTile);
				Tile next = open[0];
				var level = levels[next.level];

				// couldn't reach this tile!
				if (next.distance == int.MaxValue) break;

				open.RemoveAt(0);
				//Console.WriteLine("Tile " + next.loc + " is at distance " + next.distance);

				if (next.loc.Equals(endLoc) && next.level == 0)
				{
					return next.distance;
				}

				// don't pass through a key
				foreach (Point neighbourLoc in next.distanceTo.Keys)
				{
					Tile neighbour = levels[next.level][neighbourLoc];
					int distance = next.distance + next.distanceTo[neighbourLoc];
					if (distance < neighbour.distance) neighbour.distance = distance;

				}
				if (next.teleporter != null)
				{
					int distance = next.distance + 1;
					int neighbourLevel = (next.innerTeleporter ? next.level + 1 : next.level - 1);
					if (neighbourLevel >= 0)
					{
						if (!levels.ContainsKey(neighbourLevel))
						{
							levels[neighbourLevel] = MakeCopy(template, neighbourLevel);
							open.AddRange(levels[neighbourLevel].Values.Where(x => x.special || x.teleporter != null));
						}
						Tile neighbour = levels[neighbourLevel][next.teleportsTo];
						if (distance < neighbour.distance) neighbour.distance = distance;
						//Console.WriteLine("Teleport to " + neighbour.teleporter + " at level " + neighbour.level);
					}
				}
			}

			return int.MaxValue;
		}

		static int CompareTile(Tile a, Tile b)
		{
			return a.distance - b.distance;
		}

		static string GetTeleporter(string[] lines, int x, int y)
		{
			int lineIndex = (height - 1 - y) + 2;
			string line = lines[lineIndex];
			if (IsLetter(line[x+2-1])) return "" + line[x+2-2] + line[x+2-1];
			if (IsLetter(line[x+2+1])) return "" + line[x+2+1] + line[x+2+2];
			if (IsLetter(lines[lineIndex + 1][x + 2])) return "" + lines[lineIndex + 1][x + 2] + lines[lineIndex + 2][x + 2];
			if (IsLetter(lines[lineIndex - 1][x + 2]))
			{
				int xx = x + 2;
				int yy = lineIndex - 1;
				return "" + lines[lineIndex - 2][x + 2] + lines[lineIndex - 1][x + 2];
			}
			return null;
		}

		static bool IsLetter(char c)
		{
			int v = (int)c;
			if (65 <= v && v <= 90) Console.WriteLine(c + " is letter!");
			return 65 <= v && v <= 90;
		}
	}
}
