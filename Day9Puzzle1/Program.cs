﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day9Puzzle1
{
	class Program
	{
		static void Main(string[] args)
		{

			string text = File.ReadAllText("input.txt");
			long[] program = text.Split(',').Select(x => long.Parse(x)).ToArray();

			IntcodeComputer computer = new IntcodeComputer(program, new long[1] { 2 });
			while (!computer.IsDone())
			{
				long output = computer.CalculateProgram();
				Console.Write(output + ",");
			}

			Console.WriteLine();
			Console.WriteLine("DONE");
			Console.ReadKey();
		}
	}
}
