﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day8Puzzle1
{
	class Layer {
		public int layerId;
		public int[,] pixels;
		public int nonZero = 0;
	}

	class Program
	{
		static void Main(string[] args)
		{
			string text = File.ReadAllText("input.txt");
			int length = text.Length;
			int w = 25;//25
			int h = 6;//6
			int nLayers = length / w / h;
			Layer[] layers = new Layer[nLayers];
			int pixelId = 0;

			// convert the input into layers
			for (int i = 0; i < layers.Length; ++i)
			{
				layers[i] = new Layer() { pixels = new int[w, h], layerId = i };
				for (int y = 0; y < h; ++y)
				{
					for (int x = 0; x < w; ++x)
					{
						int pixel = int.Parse("" + text[pixelId]);
						++pixelId;
						layers[i].pixels[x, y] = pixel;
						if (pixel != 0) ++layers[i].nonZero;
					}
				}
			}

			// find the layer with the fewest nonZero digits
			Layer nonZeroLayer = layers.OrderByDescending(x => x.nonZero).First();

			// do the calculation
			int ones = 0, twos = 0;
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
					int pixel = nonZeroLayer.pixels[x, y];
					if (pixel == 1) ++ones;
					if (pixel == 2) ++twos;
				}
			}

			int calc = ones * twos;
			Console.WriteLine("The checksum is " + calc);

			// flatten the layers into the final image
			Layer image = new Layer() { pixels = new int[w, h] };
			string msg = "";
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
					bool done = false;
					for (int layerId = 0; layerId < nLayers; ++layerId)
					{
						Layer layer = layers[layerId];
						int value = layer.pixels[x, y];
						if (value != 2)
						{
							image.pixels[x, y] = value;
							Console.BackgroundColor = (value == 0 ? ConsoleColor.Black : ConsoleColor.White);
							Console.Write(" ");
							msg += (value == 0) ? "O" : " ";
							done = true;
							break;
						}
					}
					if (!done)
					{
						Console.WriteLine("ERROR ERROR");
					}
				}
				Console.WriteLine();
				msg += "\n";
			}

			Console.WriteLine(msg);
			Console.ReadKey();

		}
	}
}
