﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day24Puzzle1
{
	class Tile {
		public bool bug;
		public bool newBug;
	}

	class Level {
		public Tile[,] tiles;
	}

	class Planet
	{
		public Dictionary<int, Level> levels = new Dictionary<int, Level>();

		public int width, height;
		public int minLevel = 0, maxLevel = 0;

		public Planet(int width, int height)
		{
			this.width = width;
			this.height = height;
		}

		public int GetBugAt(int level, int x, int y)
		{
			if (!levels.ContainsKey(level))
			{
				SpawnLevel(level);
			}

			return levels[level].tiles[x, y].bug ? 1 : 0;
		}

		public Level GetLevel(int level)
		{
			if (!levels.ContainsKey(level)) SpawnLevel(level);
			return levels[level];
		}

		public void SpawnLevel(int level)
		{
			Console.WriteLine("Spawn level " + level + "...");
			Tile[,] tiles = new Tile[width, height];
			for (int yy = 0; yy < height; ++yy)
			{
				for (int xx = 0; xx < width; ++xx)
				{
					tiles[xx, yy] = new Tile() { bug = false };
				}
			}
			levels.Add(level, new Level() { tiles = tiles });

		}

		public void SetBugAt(int level)
		{
			if (level < minLevel) minLevel = level;
			if (level > maxLevel) maxLevel = level;
		}
	}

	class Program
	{
		static Planet planet;

		static int width = 5;
		static int height = 5;


		static void Main(string[] args)
		{

			string[] lines = File.ReadAllLines("input.txt");
			Tile[,] tiles = new Tile[width, height];
			for (int y = 0; y < height; ++y)
			{
				for (int x = 0; x < width; ++x)
				{
					string line = lines[height - 1 - y];
					char c = line[x];

					Tile tile = new Tile();
					tile.bug = (c == '#');
					tiles[x, y] = tile;
				}
			}
			planet = new Planet(width, height);
			planet.levels.Add(0, new Level() { tiles = tiles });
			Print(planet);

			long nLoops = 200;
			for (int i = 0; i < nLoops; ++i)
			{
				Console.WriteLine("Step " + i + "...");
				Evolve(planet);
			}

			Print(planet);

			Console.WriteLine("DONE!");

			Console.ReadKey();
		}

		static void Evolve(Planet planet)
		{
			// go over all existing levels and evolve'em
			int minLevel = planet.minLevel - 1;
			int maxLevel = planet.maxLevel + 1;
			for (int levelIndex = minLevel; levelIndex <= maxLevel; ++levelIndex) {
				Level level = planet.GetLevel(levelIndex);
				for (int y = 0; y < height; ++y)
				{
					for (int x = 0; x < width; ++x)
					{
						Tile tile = level.tiles[x, y];
						int nBugs = GetNeighbourCount(planet, levelIndex, x, y);
						if (tile.bug)
						{
							tile.newBug = (nBugs == 1);
						}
						else
						{
							tile.newBug = (nBugs == 1 || nBugs == 2);
						}
						//Console.WriteLine("Tile " + x + "," + y + " has " + tile.bug + " going to " + tile.newBug + " because " + nBugs + " neighbours");
					}
				}
			}
			for (int levelIndex = minLevel; levelIndex <= maxLevel; ++levelIndex)
			{
				Level level = planet.GetLevel(levelIndex);
				for (int y = 0; y < height; ++y)
				{
					for (int x = 0; x < width; ++x)
					{
						level.tiles[x, y].bug = level.tiles[x, y].newBug;
						if (level.tiles[x, y].bug) planet.SetBugAt(levelIndex);
					}
				}
			}
		}

		static int GetNeighbourCount(Planet planet, int level, int x, int y)
		{
			int nBugs = 0;

			// normal case
			int dx = 0;
			int dy = 1;
			for (int i = 0; i < 4; ++i)
			{
				int tmp = dx;
				dx = -dy;
				dy = tmp;
				if (x + dx < 0 || x + dx >= width) continue;
				if (y + dy < 0 || y + dy >= height) continue;

				// inner case
				if (x + dx == 2 && y + dy == 2)
				{
					continue;
				}

				// normal case
				else
				{
					nBugs += planet.GetBugAt(level, x + dx, y + dy);
				}
			}

			// outer case - we have a neighbour in a level above us
			if (x == 0) nBugs += planet.GetBugAt(level - 1, 1, 2);
			if (x == width-1) nBugs += planet.GetBugAt(level - 1, 3, 2);
			if (y == 0) nBugs += planet.GetBugAt(level - 1, 2, 1);
			if (y == height-1) nBugs += planet.GetBugAt(level - 1, 2, 3);

			// inner case
			if (x == 1 && y == 2)
			{
				nBugs += planet.GetBugAt(level + 1, 0, 0);
				nBugs += planet.GetBugAt(level + 1, 0, 1);
				nBugs += planet.GetBugAt(level + 1, 0, 2);
				nBugs += planet.GetBugAt(level + 1, 0, 3);
				nBugs += planet.GetBugAt(level + 1, 0, 4);
			}
			else if (x == 3 && y == 2)
			{
				nBugs += planet.GetBugAt(level + 1, 4, 0);
				nBugs += planet.GetBugAt(level + 1, 4, 1);
				nBugs += planet.GetBugAt(level + 1, 4, 2);
				nBugs += planet.GetBugAt(level + 1, 4, 3);
				nBugs += planet.GetBugAt(level + 1, 4, 4);
			}
			else if (x == 2 && y == 1)
			{
				nBugs += planet.GetBugAt(level + 1, 0, 0);
				nBugs += planet.GetBugAt(level + 1, 1, 0);
				nBugs += planet.GetBugAt(level + 1, 2, 0);
				nBugs += planet.GetBugAt(level + 1, 3, 0);
				nBugs += planet.GetBugAt(level + 1, 4, 0);
			}
			else if (x == 2 && y == 3) {
				nBugs += planet.GetBugAt(level + 1, 0, 4);
				nBugs += planet.GetBugAt(level + 1, 1, 4);
				nBugs += planet.GetBugAt(level + 1, 2, 4);
				nBugs += planet.GetBugAt(level + 1, 3, 4);
				nBugs += planet.GetBugAt(level + 1, 4, 4);
			}

			return nBugs;
		}

		static int GetBugCount(Planet planet, int level, int x, int y)
		{
			return planet.levels[level].tiles[x, y].bug ? 1 : 0;
		}

		static void Print(Planet planet)
		{
			int totalBugs = 0;
			for (int level = planet.minLevel; level <= planet.maxLevel; ++level)
			{
				Console.WriteLine("Level " + level);
				Tile[,] tiles = planet.levels[level].tiles;
				for (int y = height - 1; y >= 0; --y)
				{
					for (int x = 0; x < width; ++x)
					{
						if (x == 2 && y == 2) Console.Write('?');
						else
						{
							Console.Write(tiles[x, y].bug ? '#' : '.');
							if (tiles[x, y].bug) ++totalBugs;
						}
					}
					Console.WriteLine();
				}
				Console.WriteLine();
			}
			Console.WriteLine("TOTAL BUGS: " + totalBugs);
		}
	}
}
