﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day15Puzzle1
{
	class Program
	{
		class Tile {
			public Point loc;
			public TileType type;
			public Point[] path;
			public bool found = false;
			public bool filledWithOxygen = false;
			public int distance = int.MaxValue;

			public Tile(TileType type, Point loc)
			{
				this.loc = loc;
				this.type = type;
				path = new Point[] { loc };
			}

			public IEnumerable<Tile> GetNeighbours()
			{
				yield return new Tile(TileType.Unknown, new Point(loc.x + 1, loc.y)) { path = path.Append(new Point(1, 0)).ToArray() };
				yield return new Tile(TileType.Unknown, new Point(loc.x - 1, loc.y)) { path = path.Append(new Point(-1, 0)).ToArray() };
				yield return new Tile(TileType.Unknown, new Point(loc.x, loc.y + 1)) { path = path.Append(new Point(0, 1)).ToArray() };
				yield return new Tile(TileType.Unknown, new Point(loc.x, loc.y - 1)) { path = path.Append(new Point(0, -1)).ToArray() };
			}
		}

		enum TileType {
			Wall = 0,
			Empty = 1,
			Oxygen = 2,
			Unknown = 3,
		}

		static void Main(string[] args)
		{
			string text = File.ReadAllText("input.txt");
			long[] program = text.Split(',').Select(x => long.Parse(x)).ToArray();
			IntcodeComputer computer = new IntcodeComputer(program, new long[0]);

			Dictionary<Point, Tile> tiles = new Dictionary<Point, Tile>();
			List<Tile> unknownTiles = new List<Tile>();
			Point currentLoc = new Point(0, 0);
			List<Point> currentPath = new List<Point>();
			unknownTiles.Add(new Tile(TileType.Unknown, new Point(0, 1)));
			unknownTiles.Add(new Tile(TileType.Unknown, new Point(0, -1)));
			unknownTiles.Add(new Tile(TileType.Unknown, new Point(1, 0)));
			unknownTiles.Add(new Tile(TileType.Unknown, new Point(-1, 0)));
			int nExplored = 0;
			Point oxygenTank = new Point(-500000, -500000);
			while (unknownTiles.Count > 0 && nExplored < 2000)
			{
				// get the next tile we need to explore
				Tile unknownTile = unknownTiles[0];
				unknownTiles.RemoveAt(0);


				// tile already previously discovered - skip for now!
				if (tiles.ContainsKey(unknownTile.loc))
				{
					continue;
				}
				//Console.WriteLine("Explore " + unknownTile.loc);

				// now move from the origin to the tile along the defined path
				long answer = 0;
				for (int i = 0; i < unknownTile.path.Length; ++i)
				{
					int forwardDir = GetDirectionCode(unknownTile.path[i]);
					computer.AddInput(forwardDir);
					var response = computer.CalculateProgram();
					if (response.type != IntCodeResponseType.Output) throw new Exception("Expected output!");
					{
						if (i < unknownTile.path.Length-1 && response.value == 0) throw new Exception("There is a wall on the way back. This should never happen!");
						answer = response.value;
					}
				}

				// check the answer

				// we hit a wall - we did NOT move the last step!
				if (answer == 0)
				{
					unknownTile.type = TileType.Wall;
					tiles[unknownTile.loc] = unknownTile;
					currentPath = unknownTile.path.ToList();
					currentPath.RemoveAt(currentPath.Count - 1);
				}

				// we found empty space! whoo!
				else
				{
					unknownTile.type = (answer == 1 ? TileType.Empty : TileType.Oxygen);
					if (unknownTile.type == TileType.Oxygen)
					{
						oxygenTank = unknownTile.loc;
						Console.WriteLine("FOUND OXYGEN TANK AT " + unknownTile.loc);
					}
					tiles[unknownTile.loc] = unknownTile;
					currentPath = unknownTile.path.ToList();

					// explore the surroundings
					unknownTiles.AddRange(unknownTile.GetNeighbours());
				}

				// move back to the origin so we can navigate to the next tile from there
				while (currentPath.Count != 0)
				{
					Point backPoint = currentPath[currentPath.Count - 1];
					currentPath.RemoveAt(currentPath.Count - 1);
					int backDir = GetDirectionCode(new Point(-backPoint.x, -backPoint.y));
					computer.AddInput(backDir);
					var response = computer.CalculateProgram();
					if (response.type != IntCodeResponseType.Output) throw new Exception("Expected output!");
					{
						if (response.value != 1) throw new Exception("There is a wall on the way back. This should never happen!");
					}
				}

				++nExplored;
			}

			// draw the map
			Point min = new Point(tiles.Keys.Select(pp => pp.x).Min(), tiles.Keys.Select(pp => pp.y).Min());
			Point max = new Point(tiles.Keys.Select(pp => pp.x).Max(), tiles.Keys.Select(pp => pp.y).Max());
			Console.WriteLine("Move from " + min + " to " + max + " after " + nExplored + " steps");

			for (int y = max.y; y >= min.y; --y)
			{
				for (int x = min.x; x <= max.x; ++x)
				{
					Point p = new Point(x, y);
					if (!tiles.ContainsKey(p))
					{
						Console.BackgroundColor = ConsoleColor.Black;
						Console.Write("?");
						continue;
					}
					Tile tile = tiles[p];
					Console.BackgroundColor = (tile.type == TileType.Wall ? ConsoleColor.White : ConsoleColor.Black);
					if (tile.type == TileType.Oxygen) Console.BackgroundColor = ConsoleColor.Magenta;
					if (x == 0 && y == 0) Console.BackgroundColor = ConsoleColor.Blue;
					Console.Write(" ");
				}
				Console.BackgroundColor = ConsoleColor.Black;
				Console.WriteLine();
			}

			// now find the quickest path to the oxygen tank from our origin point using DIJKSTRA
			List<Tile> closed = new List<Tile>();
			List<Tile> open = new List<Tile>(tiles.Values);
			tiles[new Point(0, 0)].distance = 0;
			open = open.OrderBy((tile) => tile.distance).ToList();

			while (open.Count > 0)
			{
				Tile next = open[0];
				open.RemoveAt(0);
				//Console.WriteLine("Tile " + next.loc + " is at distance " + next.distance);

				if (next == tiles[oxygenTank])
				{
					Console.WriteLine("We found the shortest path: " + next.distance + " steps!");
					break;
				}

				closed.Add(next);

				foreach (Point neighbourLoc in next.GetNeighbours().Select(x => x.loc))
				{
					Tile neighbour = tiles[neighbourLoc];
					if (neighbour.type == TileType.Wall) continue;
					int distance = next.distance + 1;
					if (distance < neighbour.distance) neighbour.distance = distance;
					//Console.WriteLine("Tile " + neighbour.loc + " is now at distance " + distance);
				}

				open = open.OrderBy((tile) => tile.distance).ToList();
			}

			// flood the thing with oxygen using a good ol' flood fill
			tiles[oxygenTank].filledWithOxygen = true;
			int minutes = 0;
			int nFilled = 1;
			while (tiles.Values.Count(x => !x.filledWithOxygen && x.type != TileType.Wall) > 0)
			{
				List<Tile> oxygenTiles = tiles.Values.Where(x => x.type != TileType.Wall && x.filledWithOxygen && !x.found).ToList();
				foreach (Tile tile in oxygenTiles)
				{
					tile.found = true;
					//Console.WriteLine("Spread from " + tile.loc);
					foreach (Point neighbourLoc in tile.GetNeighbours().Select(x => x.loc))
					{
						Tile neighbour = tiles[neighbourLoc];
						if (neighbour.filledWithOxygen) continue;
						if (neighbour.type == TileType.Wall) continue;
						neighbour.filledWithOxygen = true;
						++nFilled;
					}
				}
				++minutes;
				//Console.WriteLine("After " + minutes + ", we filled " + nFilled);
			}

			Console.WriteLine("It took " + minutes + " minutes to fill the maze");

			Console.WriteLine("DONE!");

			Console.ReadKey();

		}

		static int GetDirectionCode(Point p) {
			if (p.x == 0 && p.y == 1) return 1;
			if (p.x == 0 && p.y == -1) return 2;
			if (p.x == -1 && p.y == 0) return 3;
			if (p.x == 1 && p.y == 0) return 4;
			throw new Exception("Invalid direction " + p);
		}
	}
}
