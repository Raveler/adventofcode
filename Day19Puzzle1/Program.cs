﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day19Puzzle1
{
	class Program
	{
		static long[] program;


		static void Main(string[] args)
		{
			string text = File.ReadAllText("input.txt");
			program = text.Split(',').Select(v => long.Parse(v)).ToArray();

			int w = 50;
			int h = 50;
			int nPulled = 0;
			int startY = 0;
			int startBeamX = int.MaxValue, endBeamX = int.MinValue;
			for (int y = 0; y < h; ++y)
			{
				startBeamX = int.MaxValue;
				endBeamX = int.MinValue;
				for (int x = 0; x < w; ++x)
				{
					bool isBeam = IsBeam(x, y);
					if (isBeam && x < startBeamX) startBeamX = x;
					if (!isBeam && startBeamX != int.MaxValue && endBeamX == int.MinValue) endBeamX = x - 1;
					if (isBeam) ++nPulled;
					Console.Write(isBeam ? "#" : ".");

				}
				Console.WriteLine();
				Console.WriteLine("On " + y + ", it goes from " + startBeamX + " to " + endBeamX);
				startY = y;
			}

			Console.WriteLine("There are " + nPulled + " pulls");

			Point p = CalculateFirstPoint(startY, startBeamX, endBeamX, 100);
			int output = p.x * 10000 + p.y;


			Console.WriteLine("The output is " + output);

			Console.ReadKey();
		}

		static Point CalculateFirstPoint(int startY, int startX, int endX, int size)
		{
			// don't bother checking y until we have reached at neast the proper width
			bool found = false;
			int y = startY;
			while (true)
			{
				// advance startX and endX
				while (!IsBeam(startX, y)) ++startX;
				while (IsBeam(endX, y)) ++endX;
				--endX;

				int w = endX - startX + 1;
				Console.WriteLine("Try line " + y + " (width " + w + ")...");
				if (w >= size)
				{
					int x = endX - size + 1;
					if (IsVerticalMatch(x, y, size))
					{
						return new Point(x, y);
					}
				}

				++y;
			}

		}

		static bool IsVerticalMatch(int x, int startY, int size)
		{
			for (int y = startY; y < startY + size; ++y)
			{
				if (!IsBeam(x, y)) return false;
			}
			return true;
		}

		static bool IsBeam(int x, int y)
		{
			IntcodeComputer drone = new IntcodeComputer(program, new long[] { x, y });
			var response = drone.CalculateProgram();
			return response.value == 1;
		}
	}
}
