﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

struct Operation
{
	public long opcode;
	public long modeParam1;
	public long modeParam2;
	public long modeParam3;
}

class Codes
{
	private long[] codes;

	public Codes(long[] program)
	{
		codes = program.ToArray();
	}

	public long this[long index]
	{
		get
		{
			EnsureProperCodesSize(index);
			return codes[index];
		}
		set
		{
			EnsureProperCodesSize(index);
			codes[index] = value;
		}
	}

	private void EnsureProperCodesSize(long outIndex)
	{
		if (outIndex >= codes.Length)
		{
			int newSize = codes.Length * 2;
			while (outIndex >= newSize) newSize *= 2;
			var newCodes = new long[newSize];
			for (int i = 0; i < codes.Length; ++i)
			{
				newCodes[i] = (i < codes.Length) ? codes[i] : 0;
			}
			codes = newCodes;
		}
	}
}

public struct IntCodeResponse
{
	public IntCodeResponseType type;
	public long value;

	public IntCodeResponse(IntCodeResponseType type, long value = 0)
	{
		this.type = type;
		this.value = value;
	}
}

public enum IntCodeResponseType
{
	Output,
	Done,
	InputNeeded
}

class IntcodeComputer
{
	private Codes codes;

	private List<long> inputs = new List<long>();
	private int inputCounter = 0;

	private List<long> outputs = new List<long>();

	private long pc = 0;

	private bool done = false;

	private long relativeBase = 0;


	public IntcodeComputer(string inputFile, long[] inputs)
	{
		long[] program = ParseProgram(inputFile);
		this.codes = new Codes(program);
		this.inputs.AddRange(inputs);
	}

	public IntcodeComputer(long[] program, long[] inputs)
	{
		this.codes = new Codes(program);
		this.inputs.AddRange(inputs);
	}

	public static long[] ParseProgram(string inputFile)
	{
		string text = File.ReadAllText("input.txt");
		var program = text.Split(',').Select(v => long.Parse(v)).ToArray();
		return program;
	}

	public void AddLine(string text)
	{
		Console.WriteLine("Add line of length " + text.Length + ": " + text);
		for (int i = 0; i < text.Length; ++i)
		{
			inputs.Add(text[i]);
		}
		inputs.Add(10);
	}

	public void AddInput(long input)
	{
		inputs.Add(input);
	}

	public void SetMemoryAddress(long address, long value)
	{
		codes[address] = value;
	}

	public long GetLastOutput()
	{
		return outputs[outputs.Count - 1];
	}

	public IntCodeResponse CalculateProgram()
	{
		// copy the code into a temp array

		while (codes[pc] != 99) // halting code
		{
			Operation operation = ParseOperation(codes[pc]);
			//Console.WriteLine("PC at " + pc + " with fullcode " + codes[pc]);
			//Console.WriteLine("Execute opcode " + operation.opcode + " with params " + operation.modeParam1 + "," + operation.modeParam2 + "," + operation.modeParam3);

			// based on the opcode, do different stuff
			if (operation.opcode == 1) ExecuteAdd(operation);
			else if (operation.opcode == 2) ExecuteMultiply(operation);
			else if (operation.opcode == 3)
			{
				// we return if we need input for this operation but don't have it
				if (!ExecuteInput(operation)) return new IntCodeResponse(IntCodeResponseType.InputNeeded);
			}
			else if (operation.opcode == 4) return new IntCodeResponse(IntCodeResponseType.Output, ExecuteOutput(operation));
			else if (operation.opcode == 5) ExecuteJump(operation, true);
			else if (operation.opcode == 6) ExecuteJump(operation, false);
			else if (operation.opcode == 7) ExecuteLessThan(operation);
			else if (operation.opcode == 8) ExecuteEquals(operation);
			else if (operation.opcode == 9) UpdateRelativeBase(operation);
			else throw new Exception("ERROR ERROR");
		}

		done = true;
		return new IntCodeResponse(IntCodeResponseType.Done);
	}

	public bool IsDone()
	{
		return done;
	}

	private long GetValue(long paramMode, long pc) {
		long value = codes[pc];
		if (paramMode == 0) value = codes[value];
		if (paramMode == 2) value = codes[relativeBase + value];
		return value;
	}


	private void SetValue(long pc, long paramMode, long value)
	{
		long outIndex = codes[pc];

		// resize the codes array
		if (paramMode == 0) codes[outIndex] = value;
		else if (paramMode == 2) codes[relativeBase + outIndex] = value;
		else throw new Exception("Cannot execute an output instruction in immediate mode.");
	}

	private void ExecuteAdd(Operation op)
	{
		long param1 = GetValue(op.modeParam1, pc+1);
		long param2 = GetValue(op.modeParam2, pc+2);
		SetValue(pc + 3, op.modeParam3, param1 + param2);
		pc += 4;
	}

	private void ExecuteMultiply(Operation op)
	{
		long param1 = GetValue(op.modeParam1, pc + 1);
		long param2 = GetValue(op.modeParam2, pc + 2);
		SetValue(pc + 3, op.modeParam3, param1 * param2);
		pc += 4;
	}

	private void ExecuteJump(Operation op, bool expectedResult)
	{
		if ((GetValue(op.modeParam1, pc + 1) != 0) == expectedResult) pc = GetValue(op.modeParam2, pc + 2);
		else pc += 3;
	}

	private void ExecuteLessThan(Operation op)
	{
		long param1 = GetValue(op.modeParam1, pc + 1);
		long param2 = GetValue(op.modeParam2, pc + 2);
		SetValue(pc + 3, op.modeParam3, param1 < param2 ? 1 : 0);
		pc += 4;
	}

	private void ExecuteEquals(Operation op)
	{
		long param1 = GetValue(op.modeParam1, pc + 1);
		long param2 = GetValue(op.modeParam2, pc + 2);
		SetValue(pc + 3, op.modeParam3, param1 == param2 ? 1 : 0);
		pc += 4;
	}

	private bool ExecuteInput(Operation op)
	{
		// if there are no inputs, we return a command
		if (inputCounter >= inputs.Count) return false;

		// read the input
		long value = inputs[inputCounter];
		inputCounter++;
		SetValue(pc + 1, op.modeParam1, value);
		pc += 2;
		return true;
	}

	private long ExecuteOutput(Operation op)
	{
		long value = GetValue(op.modeParam1, pc + 1);
		outputs.Add(value);
		pc += 2;
		return value;
	}

	private void UpdateRelativeBase(Operation op)
	{
		long value = GetValue(op.modeParam1, pc + 1);
		relativeBase += value;
		pc += 2;
	}

	private Operation ParseOperation(long code)
	{
		// https://adventofcode.com/2019/day/5 table
		long E = code % 10; code /= 10;
		long D = code % 10; code /= 10;
		long C = code % 10; code /= 10;
		long B = code % 10; code /= 10;
		long A = code % 10; code /= 10;

		return new Operation
		{
			opcode = D * 10 + E,
			modeParam1 = C,
			modeParam2 = B,
			modeParam3 = A,
		};
	}
}
