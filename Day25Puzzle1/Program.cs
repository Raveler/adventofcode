﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day25Puzzle1
{
	class Tile {
		public Point loc;
		public List<string> items;
	}

	public class Response
	{
		public IntCodeResponseType type;
		public string output;
	}

	class Program
	{
		static IntcodeComputer droid;

		static Point loc = new Point(0, 0);

		static string[] fullInventory = new string[] {
			"antenna", "ornament", "fixed point", "asterisk", "astronaut ice cream", "dark matter", "monolith", "hologram",
		};

		static string[] startCommands = new string[] {
			"east", "take antenna", "east", "take ornament", "north", "west", "take fixed point",
			"east", "south", "west", "north", "north", "take asterisk",
			"south", "west", "south", "take hologram", "north", "west", "take astronaut ice cream",
			"east", "east", "south", "west", "south", "south", "south", "take dark matter",
			"north", "west", "north", "take monolith", "north", "north", "east" };

		static void Main(string[] args)
		{
			long[] program = IntcodeComputer.ParseProgram("input.txt");
			droid = new IntcodeComputer(program, new long[0]);
			List<string> inventory = new List<string>();

			List<string> remainingCommands = new List<string>();
			List<string> inventoryCommands = GenerateInventoryCommands(0, new bool[fullInventory.Length]);
			remainingCommands.AddRange(startCommands);
			Response response;
			bool checkingInventory = false;
			while ((response = Proceed(droid)).type != IntCodeResponseType.Done)
			{
				if (checkingInventory)
				{
					string text = response.output;
					if (!text.Contains("Alert!"))
					{
						Console.WriteLine("OHHH!!!!!");
					}
					checkingInventory = false;
				}

				if (remainingCommands.Count > 0)
				{
					Console.WriteLine(remainingCommands[0]);
					EnterCommand(remainingCommands[0]);
					remainingCommands.RemoveAt(0);
				}
				else if (inventoryCommands.Count > 0)
				{
					string cmd = inventoryCommands[0];
					Console.WriteLine(cmd);
					EnterCommand(cmd);
					inventoryCommands.RemoveAt(0);
					if (cmd == "east")
					{
						checkingInventory = true;
					}
				}
				else
				{
					string cmd = Console.ReadLine();
					EnterCommand(cmd);
				}
			}

			Console.WriteLine("DONE!");
			Console.ReadKey();
		}

		static List<string> GenerateInventoryCommands(int idx, bool[] inventory)
		{
			// end condition
			if (idx >= fullInventory.Length)
			{
				List<string> commands = new List<string>();
				for (int i = 0; i < inventory.Length; ++i)
				{
					if (!inventory[i]) commands.Add("drop " + fullInventory[i]);
				}
				commands.Add("east");
				for (int i = 0; i < inventory.Length; ++i)
				{
					if (!inventory[i]) commands.Add("take " + fullInventory[i]);
				}
				return commands;
			}

			List<string> allCommands = new List<string>();
			inventory[idx] = false;
			var noCommands = GenerateInventoryCommands(idx + 1, inventory);
			inventory[idx] = true;
			var yesCommands = GenerateInventoryCommands(idx + 1, inventory);
			allCommands.AddRange(noCommands);
			allCommands.AddRange(yesCommands);
			return allCommands;
		}

		static void EnterCommand(string cmd) {
			for (int i = 0; i < cmd.Length; ++i)
			{
				droid.AddInput(cmd[i]);
			}
			droid.AddInput(10);
		}

		static Response Proceed(IntcodeComputer droid)
		{
			StringBuilder ss = new StringBuilder();
			IntCodeResponse response = droid.CalculateProgram();
			while (response.type == IntCodeResponseType.Output)
			{
				Console.Write((char)response.value);
				ss.Append((char)response.value);
				response = droid.CalculateProgram();
			}
			return new Response()
			{
				type = response.type,
				output = ss.ToString(),
			};
		}
	}
}
