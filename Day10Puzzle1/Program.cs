﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10Puzzle1
{
	class Spot
	{
		public bool asteroid;
		public int score;
		public int x, y;
	}

	class Program
	{
		static int w, h;
		static Spot[,] space;

		static void Main(string[] args)
		{
			string[] lines = File.ReadAllLines("input.txt");
			h = lines.Length;
			w = lines[0].Length;

			// fill up space
			space = new Spot[w, h];
			for (int y = 0; y < h; ++y)
			{
				string line = lines[y];
				for (int x = 0; x < w; ++x)
				{
					space[x, y] = new Spot()
					{
						asteroid = (line[x] == '#'),
						score = 0,
						x = x,
						y = y
					};
				}
			}

			// calculate score for each asteroid
			int bestScore = int.MinValue;
			int stationX = -1, stationY = -1;
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
					if (space[x, y].asteroid)
					{
						space[x, y].score = CalculateAsteroidsSeen(x, y);
						if (space[x, y].score > bestScore)
						{
							bestScore = space[x, y].score;
							stationX = x;
							stationY = y;
						}
					}
				}
			}

			Console.WriteLine("The best score is " + bestScore + " at " + stationX + "," + stationY);

			// now start zapping asteroids in clockwise order, starting up
			IEnumerable<Spot> visibleAsteroids = GetVisibleSpots(stationX, stationY);
			int nZapped = 0;
			int nTargetZapped = 200;
			bool done = false;
			while (visibleAsteroids.Count() > 0 && !done)
			{
				Console.WriteLine("First step: " + visibleAsteroids.Count() + " to zap!");
				// sort them by their angle
				visibleAsteroids = visibleAsteroids.OrderByDescending(spot =>
				{
					double angle = Math.Atan2(-(spot.y - stationY), spot.x - stationX);
					if (angle > Math.PI / 2.0) angle -= Math.PI * 2; // make sure that 90° is the lowest number
					//Console.WriteLine("Spot " + spot.x + "," + spot.y + " is at angle " + (angle / Math.PI * 180.0));
					return angle;
				});

				// zap them in order
				foreach (Spot spot in visibleAsteroids)
				{
					++nZapped;
					Console.WriteLine("Zap " + spot.x + "," + spot.y + " as " + nZapped + "th");
					space[spot.x, spot.y].asteroid = false;
					if (nZapped == nTargetZapped)
					{
						Console.WriteLine("THIS WAS THE " + nTargetZapped + "th asteroid!");
						done = true;
						break;
					}
				}

				visibleAsteroids = GetVisibleSpots(stationX, stationY);
			}

			Console.ReadKey();


		}

		private static IEnumerable<Spot> GetVisibleSpots(int baseX, int baseY)
		{
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
					if (baseX == x && baseY == y) continue;
					if (space[x, y].asteroid && CanBeSeenFrom(x, y, baseX, baseY))
					{
						yield return space[x, y];
					}
				}
			}
		}

		private static int CalculateAsteroidsSeen(int baseX, int baseY)
		{
			//Console.WriteLine("INVESTIGATE " + baseX + "," + baseY + ":");

			// go over all asteroids and see if we can see them
			int score = 0;
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
					if (baseX == x && baseY == y) continue;
					if (space[x, y].asteroid && CanBeSeenFrom(x, y, baseX, baseY))
					{
						++score;
					}
				}
			}
			return score;
		}

		private static bool CanBeSeenFrom(int targetX, int targetY, int baseX, int baseY)
		{
			int dx = targetX - baseX;
			int dy = targetY - baseY;

			// find each division that results in whole numbers for both dx/dy
			bool collision = false;
			for (int div = 2; div <= Math.Max(Math.Abs(dx), Math.Abs(dy)); ++div)
			{
				// this is a good division! see if we hit any space object on the way
				if (dx % div == 0 && dy % div == 0)
				{
					//Console.WriteLine("dx,dy " + dx + "," + dy + " has divisor " + div);
					for (int i = 1; i < div; ++i)
					{
						if (space[baseX + dx / div * i, baseY + dy / div * i].asteroid) collision = true;
					}
				}
			}

			//Console.WriteLine("Can " + targetX + "," + targetY + " (dx " + dx + "," + dy + ") be seen from " + baseX + "," + baseY + ": " + (!collision));
			return !collision;
		}
	}
}
