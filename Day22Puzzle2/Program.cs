﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day22Puzzle1
{
	public enum Technique
	{
		DealIntoNewStack,
		Cut,
		DealWithIncrement
	}

	public class Action {
		public Technique technique;
		public int value;
		public long offset;
		public int multiplier;
	}

	public class Loop {
		public long start;
		public long length;
	}

	public class Polynomial
	{
		public BigInteger a;
		public BigInteger b;
		public BigInteger mod;
		public BigInteger nCards;

		public BigInteger Eval(BigInteger cardToFollow, long nCycles = 1)
		{
			BigInteger val = cardToFollow;
			for (long i = 0; i < nCycles; ++i)
			{
				val = (a * cardToFollow + b + mod) % mod;
				//while (val < 0) val += nCards;
				if (mod < 0) val = nCards - 1 + val;
				Console.WriteLine("Card " + cardToFollow + " is mapped to " + val);
			}
			return val;
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			string fileName = "input.txt";
			long nCards = 119315717514047;
			long cardToFollow = 2020;
			long nCycles = 101741582076661;

			/*string fileName = "easy-input.txt";
			long nCards = 10;
			long cardToFollow = 7;
			long nCycles = 11;*/

			string[] lines = File.ReadAllLines(fileName);
			List<Action> actions = new List<Action>();
			Regex regex = new Regex("[0-9-]+");
			int multiplier = 1;
			long offset = 0;
			for (int i = 0; i < lines.Length; ++i)
			{
				string line = lines[i];

				Technique technique = Technique.DealIntoNewStack;
				if (line.Contains("cut"))
				{
					technique = Technique.Cut;
				}
				else if (line.Contains("increment"))
				{
					technique = Technique.DealWithIncrement;
				}
				else
				{
					technique = Technique.DealIntoNewStack;
				}

				int value = -1;
				if (technique != Technique.DealIntoNewStack)
				{
					value = int.Parse(regex.Match(line).Value);
				}

				/*if (technique == Technique.DealIntoNewStack)
				{
					multiplier = -multiplier;
					continue;
				}*/
				/*if (technique == Technique.Cut)
				{
					offset += value;
					continue;
				}*/


				Action action = new Action()
				{
					technique = technique,
					value = value,
					offset = offset,
					multiplier = multiplier,
				};
				actions.Add(action);
			}

			if (multiplier == -1) cardToFollow = nCards - 1 - cardToFollow;

			long originalCard = cardToFollow;

			// now invert the actions
			List<Action> invertedActions = new List<Action>();
			for (int i = actions.Count - 1; i >= 0; --i)
			{
				Action action = actions[i];
				Action newAction = new Action()
				{
					technique = action.technique,
					value = -action.value,
					offset = action.offset,
					multiplier = action.multiplier,
				};
				invertedActions.Add(newAction);
			}
			List<Action> originalActions = actions;
			actions = invertedActions;

			//long finalValue = PerformShuffle(actions, nCards, cardToFollow, nCycles);

			// perform the shuffle once and twice to get two formulas
			// solve the equations AX + B = Y, AY + B = Z
			long X = cardToFollow;
			BigInteger Y = PerformShuffle(invertedActions, nCards, X, 1);
			BigInteger Z = PerformShuffle(invertedActions, nCards, X, 2);
			BigInteger A = (Y - Z) * ModInv(X - Y + nCards, nCards);
			BigInteger B = Y - A * X;


			// first, determine the final value after the total number of loops
			/*long finalValueAtPosition = PerformShuffle(invertedActions, nCards, cardToFollow, nCycles);
			Console.WriteLine("After " + nCycles + " inverted cycles, we end up with the original card " + finalValueAtPosition + " at position " + cardToFollow);
			long originalCardToFollow = finalValueAtPosition;
			long finalPosition = PerformShuffle(originalActions, nCards, originalCardToFollow, nCycles);
			Console.WriteLine("After " + nCycles + ", original card " + originalCardToFollow + " is at " + finalPosition);
			long polyValue = originalCard;
			for (int i = 0; i < nCycles; ++i)
			{
				polyValue = (A * polyValue + B + nCards) % nCards;
				Console.WriteLine("POLY card " + originalCard + " is at " + polyValue + " after " + i);
			}
			Console.WriteLine("After " + nCycles + " inverted poly applications, original card " + polyValue + " at position " + cardToFollow);
			*/

			// the formula of the series f^n(cardToFollow) is this:
			//BigInteger bigPolyPosition = (BigInteger.ModPow(A, nCycles, nCards) * X) + (BigInteger.ModPow(A, nCycles, nCards) - 1) / (A - 1) * B;
			var PA = BigInteger.ModPow(A, nCycles, nCards);
			var BM = (PA - 1) * ModInv(A - 1, nCards);
			BigInteger bigPolyValue = (PA * X) + (BM * B) % nCards;
			bigPolyValue = bigPolyValue % nCards;
			while (bigPolyValue < 0) bigPolyValue += nCards;
			while (bigPolyValue >= nCards) bigPolyValue -= nCards;
			Console.WriteLine("After " + nCycles + " inverted BIG poly applications, original card " + bigPolyValue + " at position " + cardToFollow);

			// do the actions
			/*long[] finalCards = new long[nCards];
			long[] finalInvertedCards = new long[nCards];
			long[] polyCards = new long[nCards];
			Polynomial poly = CreatePolynomial(nCards, originalActions);
			for (int i = 0; i < nCards; ++i)
			{
				long finalPos = PerformShuffle(originalActions, nCards, i, nCycles);
				long finalValue = PerformShuffle(invertedActions, nCards, i, nCycles);
				finalCards[finalPos] = i;
				finalInvertedCards[i] = finalValue;
				//long polyPos = (int)poly.Eval(i);
				long polyPos = (A * i + B + nCards) % nCards;
				while (polyPos < 0) polyPos += nCards;
				while (polyPos >= nCards) polyPos -= nCards;
				Console.WriteLine("Card " + i + " ends at " + finalPos + "/" + polyPos);
				polyCards[polyPos] = i;
			}

			Console.Write("FORWARD: ");
			for (int i = 0; i < nCards; ++i)
			{
				Console.Write(finalCards[i] + " ");
			}
			Console.WriteLine();
			Console.Write("REVERT:  ");
			for (int i = 0; i < nCards; ++i)
			{
				Console.Write(finalInvertedCards[i] + " ");
			}
			Console.WriteLine();
			Console.Write("POLY:    ");
			for (int i = 0; i < nCards; ++i)
			{
				Console.Write(polyCards[i] + " ");
			}
			Console.WriteLine();*/

			Console.WriteLine("DONE!");
			Console.ReadKey();
		}

		private static BigInteger ModInv(BigInteger v, BigInteger n)
		{
			return BigInteger.ModPow(v, n - 2, n);
		}

		private static long ApplyAction(long nCards, long cardToFollow, Action action, bool lastAction)
		{
			if (action.technique == Technique.DealIntoNewStack) cardToFollow = DealIntoNewStack(nCards, cardToFollow);
			else if (action.technique == Technique.Cut) cardToFollow = Cut(nCards, action.value, action.multiplier, cardToFollow);
			else cardToFollow = DealWithIncrement(nCards, action.value, action.offset, action.multiplier, cardToFollow);
			if (lastAction && action.multiplier < 0) cardToFollow = nCards - 1 - cardToFollow;
			return cardToFollow;
		}

		private static long PerformShuffle(List<Action> actions, long nCards, long cardToFollow, long nTimes)
		{
			long originalCard = cardToFollow;
			HashSet<long> offsets = new HashSet<long>();
			Console.WriteLine("EXECUTE " + nTimes + " TIMES:");
			for (long n = 0; n < nTimes; ++n)
			{
				//if (n % 10000 == 0) Console.WriteLine("After " + n + " iterations...");
				for (int i = 0; i < actions.Count; ++i)
				{
					Action action = actions[i];
					cardToFollow = ApplyAction(nCards, cardToFollow, action, i == actions.Count-1);
				}
				Console.WriteLine("Card " + originalCard + " is at " + cardToFollow + " after " + n);
			}


			return cardToFollow;
		}

		static long DealIntoNewStack(long cardCount, long cardToFollow)
		{
			// invert
			return cardCount - 1 - cardToFollow;
		}

		static long Cut(long cardCount, int value, int multiplier, long cardToFollow)
		{
			// shift!
			if (multiplier < 0) value = -value;
			return (cardToFollow - value + cardCount) % cardCount;
		}

		static long DealWithIncrement(long cardCount, int n, long offset, int multiplier, long cardToFollow)
		{
			// if there is a multiplier, we invert the cardToFollow
			if (multiplier < 0) cardToFollow = cardCount - 1 - cardToFollow;

			// ??
			if (n > 0)
			{
				cardToFollow -= offset;
				if (cardToFollow < 0) cardToFollow = cardCount + cardToFollow;
				cardToFollow = (n * cardToFollow + cardCount) % cardCount;
			}
			else
			{
				// THIS IS WHAT SHOULD HAPPEN WHEN THE MULTIPLIER IS -1:
				/*cardToFollow = cardCount - 1 - cardToFollow;
				cardToFollow = (Math.Abs(n) * cardToFollow + cardCount) % cardCount;
				cardToFollow = cardCount - 1 - cardToFollow;
				return cardToFollow;*/

				// THIS IS WHAT SHOULD HAPPEN WHEN REVERTING THE ORDER OF OPERATIONS:
				cardToFollow -= offset;
				if (cardToFollow < 0) cardToFollow = cardCount + cardToFollow;
				while (cardToFollow % n != 0) cardToFollow += cardCount;
				if (cardToFollow < 0) throw new Exception("Should not happen.");
				cardToFollow = cardToFollow / Math.Abs(n);
				return cardToFollow;
			}

			if (multiplier < 0) cardToFollow = cardCount - 1 - cardToFollow;

			return cardToFollow;
		}

		static Polynomial CreatePolynomial(long nCards, List<Action> actions)
		{
			Polynomial poly = new Polynomial() { a = 1, b = 0, mod = nCards, nCards = nCards };
			for (int i = 0; i < actions.Count; ++i)
			{
				AddActionToPolynomial(poly, nCards, actions[i]);
			}
			return poly;
		}

		static void AddActionToPolynomial(Polynomial poly, long nCards, Action action)
		{
			if (action.technique == Technique.DealIntoNewStack)
			{
				poly.a = -poly.a;
				poly.b = -poly.b;
				poly.mod = -poly.mod;
			}
			else if (action.technique == Technique.Cut)
			{
				int n = action.value;
				poly.b = (poly.b - n);
			}
			else
			{
				int n = action.value;
				if (n > 0)
				{
					poly.a *= n;
					poly.b *= n;
					//poly.mod *= n;
				}
				else
				{
					throw new NotSupportedException();
				}
			}

			while (poly.b < 0) poly.b += nCards;
			while (poly.b >= nCards) poly.b -= nCards;
		}
	}
}
