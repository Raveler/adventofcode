﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5Puzzle1
{
	struct Operation
	{
		public int opcode;
		public int modeParam1;
		public int modeParam2;
		public int modeParam3;
	}

	class IntcodeComputer
	{
		private int[] codes;

		private int[] inputs;
		private int inputCounter = 0;

		private List<int> outputs = new List<int>();

		private int pc = 0;

		public IntcodeComputer(int[] program, int[] inputs)
		{
			this.codes = program.ToArray();
			this.inputs = inputs.ToArray();
		}

		public int[] CalculateProgram()
		{
			// copy the code into a temp array

			while (codes[pc] != 99) // halting code
			{
				Operation operation = ParseOperation(codes[pc]);
				Console.WriteLine("PC at " + pc + " with fullcode " + codes[pc]);
				Console.WriteLine("Execute opcode " + operation.opcode + " with params " + operation.modeParam1 + "," + operation.modeParam2 + "," + operation.modeParam3);

				// based on the opcode, do different stuff
				if (operation.opcode == 1) ExecuteAdd(operation);
				else if (operation.opcode == 2) ExecuteMultiply(operation);
				else if (operation.opcode == 3) ExecuteInput(operation);
				else if (operation.opcode == 4) ExecuteOutput(operation);
				else if (operation.opcode == 5) ExecuteJump(operation, true);
				else if (operation.opcode == 6) ExecuteJump(operation, false);
				else if (operation.opcode == 7) ExecuteLessThan(operation);
				else if (operation.opcode == 8) ExecuteEquals(operation);
				else throw new Exception("ERROR ERROR");
			}

			return outputs.ToArray();
		}

		private int GetValue(int paramMode, int pc) {
			int value = codes[pc];
			if (paramMode == 0) value = value = codes[value];
			return value;
		}

		private void SetValue(int pc, int value) {
			int outIndex = codes[pc];
			codes[outIndex] = value;
		}

		private void ExecuteAdd(Operation op)
		{
			int param1 = GetValue(op.modeParam1, pc+1);
			int param2 = GetValue(op.modeParam2, pc+2);
			SetValue(pc + 3, param1 + param2);
			pc += 4;
		}

		private void ExecuteMultiply(Operation op)
		{
			int param1 = GetValue(op.modeParam1, pc + 1);
			int param2 = GetValue(op.modeParam2, pc + 2);
			SetValue(pc + 3, param1 * param2);
			pc += 4;
		}

		private void ExecuteJump(Operation op, bool expectedResult)
		{
			if ((GetValue(op.modeParam1, pc + 1) != 0) == expectedResult) pc = GetValue(op.modeParam2, pc + 2);
			else pc += 3;
		}

		private void ExecuteLessThan(Operation op)
		{
			int param1 = GetValue(op.modeParam1, pc + 1);
			int param2 = GetValue(op.modeParam2, pc + 2);
			SetValue(pc + 3, param1 < param2 ? 1 : 0);
			pc += 4;
		}

		private void ExecuteEquals(Operation op)
		{
			int param1 = GetValue(op.modeParam1, pc + 1);
			int param2 = GetValue(op.modeParam2, pc + 2);
			SetValue(pc + 3, param1 == param2 ? 1 : 0);
			pc += 4;
		}

		private void ExecuteInput(Operation op)
		{
			int value = inputs[inputCounter];
			inputCounter++;

			int outIndex = codes[pc + 1];
			codes[outIndex] = value;
			//Console.WriteLine("INPUT: " + value);

			pc += 2;
		}

		private void ExecuteOutput(Operation op)
		{
			int op1Index = codes[pc + 1];

			int value = codes[op1Index];
			if (op.modeParam1 == 1) value = op1Index;

			//Console.WriteLine("OUT: " + value);
			outputs.Add(value);

			pc += 2;
		}

		private Operation ParseOperation(int code)
		{
			// https://adventofcode.com/2019/day/5 table
			int E = code % 10; code /= 10;
			int D = code % 10; code /= 10;
			int C = code % 10; code /= 10;
			int B = code % 10; code /= 10;
			int A = code % 10; code /= 10;

			return new Operation
			{
				opcode = D * 10 + E,
				modeParam1 = C,
				modeParam2 = B,
				modeParam3 = A,
			};
		}
	}
}
