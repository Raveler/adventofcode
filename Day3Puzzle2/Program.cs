﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3Puzzle1
{
	struct Point: IEquatable<Point>
	{
		public int x;
		public int y;

		public bool Equals(Point other)
		{
			return x == other.x && y == other.y;
		}

		public override int GetHashCode()
		{
			return (x, y).GetHashCode();
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			string[] lines = File.ReadAllLines("testInput.txt");
			string line1 = lines[0];
			string line2 = lines[1];

			string[] actions = line1.Split(',');
			Dictionary<Point, int> occupied = new Dictionary<Point, int>();
			Point current = new Point() { x = 0, y = 0 };
			int travelDistance = 0;

			// loop over all actions and execute them and store them in occupied
			for (int i = 0; i < actions.Length; ++i)
			{
				string action = actions[i]; // eg R60

				// extract the dir
				char dirCode = action[0];
				Point dir = GetDir(dirCode);

				int amount = int.Parse(action.Substring(1));

				// move in the dir
				for (int step = 0; step < amount; ++step)
				{
					current.x += dir.x;
					current.y += dir.y;
					++travelDistance;

					//Console.WriteLine("Visited " + current.x + "," + current.y);
					occupied[current] = travelDistance;
				}
			}
			Console.WriteLine("There are " + occupied.Count + " occupied spots");

			// do the same for the second wire, but mark the crossings
			current = new Point() { x = 0, y = 0 };
			travelDistance = 0;
			Dictionary<Point, int> crossings = new Dictionary<Point, int>();
			actions = line2.Split(',');
			for (int i = 0; i < actions.Length; ++i)
			{
				string action = actions[i]; // eg R60

				// extract the dir
				char dirCode = action[0];
				Point dir = GetDir(dirCode);

				int amount = int.Parse(action.Substring(1));

				// move in the dir
				for (int step = 0; step < amount; ++step)
				{
					current.x += dir.x;
					current.y += dir.y;
					++travelDistance;
					//Console.WriteLine("Visited " + current.x + "," + current.y);

					if (occupied.ContainsKey(current))
					{
						//Console.WriteLine("Crossed at " + current.x + "," + current.y + "!!!");
						crossings[current] = travelDistance + occupied[current];
					}
				}
			}
			Console.WriteLine("There are " + crossings.Count + " crossings");

			int minDistance = int.MaxValue;
			Point bestCrossing = new Point() { x = 0, y = 0 };
			foreach (Point crossing in crossings.Keys)
			{
				int distance = crossings[crossing];
				if (distance < minDistance)
				{
					bestCrossing = crossing;
					minDistance = distance;
				}
			}

			Console.WriteLine("The best crossing is at distance " + minDistance + ": " + bestCrossing.x + "," + bestCrossing.y);

			Console.ReadKey();
		}

		static Point GetDir(char dirCode)
		{
			switch (dirCode)
			{
				case 'U':
					return new Point() { x = 0, y = 1 };
				case 'D':
					return new Point() { x = 0, y = -1 };
				case 'L':
					return new Point() { x = -1, y = 0 };
				case 'R':
					return new Point() { x = 1, y = 0 };
			}
			throw new Exception("ERROR ERROR");
		}
	}
}
