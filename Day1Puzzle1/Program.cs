﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1Puzzle1
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] lines = File.ReadAllLines("input.txt");
			int sum = 0;
			for (int i = 0; i < lines.Length; ++i)
			{
				int mass = int.Parse(lines[i]);
				int fuel = CalculateFuel(mass);
				sum += fuel;
			}
			Console.WriteLine(sum);
			Console.ReadKey();
		}

		static int CalculateFuel(int mass)
		{
			int fuel = ((int)Math.Floor(mass / 3.0)) - 2;
			if (fuel < 0) return 0; // wishing really hard
			return fuel + CalculateFuel(fuel);
		}
	}
}
